<%@tag description="Overall Page template" pageEncoding="UTF-8" %>
<%@attribute name="content" fragment="true" %>
<%@attribute name="sidebar" fragment="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:bundle basename="i18n">
    <fmt:message key="nav.label.about" var="about"/>
    <fmt:message key="nav.label.catalog" var="catalog"/>
    <fmt:message key="nav.label.home" var="home"/>
    <fmt:message key="base.label.authorize" var="authorize"/>
    <fmt:message key="registration.label.email" var="email"/>
    <fmt:message key="registration.label.password" var="password"/>
    <fmt:message key="base.button.submit" var="submit"/>
    <fmt:message key="registration.label.register" var="registerLabel"/>
    <fmt:message key="base.nav.adminPage" var="adminPage"/>
    <fmt:message key="base.nav.orderManager" var="orderManager"/>
    <fmt:message key="base.nav.catalogManager" var="catalogManager"/>
    <fmt:message key="base.nav.myProfile" var="myProfile"/>
    <fmt:message key="base.nav.logout" var="logout"/>
    <fmt:message key="registration.label.enter" var="enter"/>
    <fmt:message key="main.menu" var="menu"/>
    <fmt:message key="main.menu.renter" var="renter"/>
    <fmt:message key="main.menu.how_rent" var="how_rent"/>
    <fmt:message key="main.menu.requirement_for_tenants" var="requirement_for_tenants"/>
    <fmt:message key="main.menu.emergencies" var="emergencies"/>
    <fmt:message key="main.menu.advice" var="advice"/>
    <fmt:message key="main.menu.for_distributors" var="for_distributors"/>
    <fmt:message key="main.menu.how_to_rent_out_a_car" var="how_to_rent_out_a_car"/>
    <fmt:message key="main.menu.safety" var="safety"/>
    <fmt:message key="main.menu.sublease" var="sublease"/>
    <fmt:message key="main.menu.about_us" var="about_us"/>
    <fmt:message key="main.menu.contacts" var="contacts"/>
    <fmt:message key="main.menu.blog" var="blog"/>
    <fmt:message key="main.menu.franchise" var="franchise"/>
    <fmt:message key="main.menu.agreements_and_rules" var="agreements_and_rules"/>
    <fmt:message key="main.menu.contract_template" var="contract_template"/>
    <fmt:message key="main.menu.admin_menu" var="admin_menu"/>
    <fmt:message key="main.menu.show_users" var="show_users"/>
    <fmt:message key="main.menu.show_cars" var="show_cars"/>
    <fmt:message key="main.menu.booking_info" var="booking_info"/>
    <fmt:message key="main.menu.logout" var="logout"/>
    <fmt:message key="main.menu.help_info" var="help_info"/>
    <fmt:message key="main.menu.subscribe_to_news" var="subscribe_to_news"/>
    <fmt:message key="main.menu_customer_support" var="customer_support"/>
    <fmt:message key="main.menu.hand_over_car" var="hand_over_car"/>
    <fmt:message key="user.menu.my_car" var="my_car"/>
    <fmt:message key="user.menu.my_orders" var="my_orders"/>
    <fmt:message key="user.menu.add_car" var="add_car"/>
    <fmt:message key="user.menu.my_reservations" var="my_reservations"/>
    <fmt:message key="user.main.settings" var="settings"/>

    <fmt:message key="base.nav.myOrders" var="myOrders"/>
    <fmt:message key="base.nav.myCart" var="myCart"/>
</fmt:bundle>
<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:700,400&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/css/bootstrap-slider.css" rel="stylesheet">
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap-timepicker.min.js"></script>
    <script src="../js/script.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/bootstrap-slider.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
</head>

<body>

<header>
    <div class="menu-top">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <a class="navbar-brand" href="home-page">
                        <img src="../../img/logo-1.png" alt="">
                    </a>
                    <ul class="nav navbar-nav">
                        <li class="dropdown my-dropdown-meny">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-th-list my-menu"><img src="img/flag.jpg" alt=""></span>Menu <span class="caret"></span></a>
                            <ul class="dropdown-menu my-dropdown-meny2">
                                <div class="container my-container">
                                    <div class="row my-row-1" >
                                        <div class="col-md-4 my-col-md-4">
                                            <div class="row my-dropdown-meny2-row-logo"><div class="glyphicon glyphicon-briefcase"></div></div>
                                            <div class="row"><span class="ar_menu_subtitle">${renter}</span></div>
                                            <div class="row my-row-li">
                                                <ul class="list-group">
                                                    <li class="list-group-item"><a href="#">${how_rent}</a></li>
                                                    <li class="list-group-item"><a href="#">${requirement_for_tenants}</a></li>
                                                    <li class="list-group-item" ><a href="#">${emergencies}</a></li>
                                                    <li class="list-group-item"><a href="#">${advice}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4 my-col-md-4">
                                            <div class="row my-dropdown-meny2-row-logo"><div class="glyphicon glyphicon-briefcase"></div></div>
                                            <div class="row"><span class="ar_menu_subtitle">${for_distributors}</span></div>
                                            <div class="row my-row-li">
                                                <ul class="list-group">
                                                    <li class="list-group-item"><a href="#">${how_to_rent_out_a_car}</a></li>
                                                    <li class="list-group-item"><a href="#">${safety}</a></li>
                                                    <li class="list-group-item" ><a href="#">${sublease}</a></li>
                                                    <li class="list-group-item"><a href="#">${advice}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4 my-col-md-4">
                                            <div class="row my-row-li2">
                                                <ul class="list-group">
                                                    <li class="list-group-item"><a href="#">${about_us}</a></li>
                                                    <li class="list-group-item"><a href="#">${contacts}</a></li>
                                                    <li class="list-group-item" ><a href="#">${blog}</a></li>
                                                    <li class="list-group-item"><a href="#">${franchise}</a></li>
                                                    <li class="list-group-item" ><a href="#">${agreements_and_rules}</a></li>
                                                    <li class="list-group-item"><a href="#">${contract_template}</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </ul>
                        </li>
                        <li><a href="myauto">${hand_over_car}</a></li>
                        <c:if test = "${sessionScope.userAttributes.role.equals('Admin')}">
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">${admin_menu}
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="allUsersPageAdmin">${show_users}</a></li>
                                        <li><a href="allCarsPageAdmin">${show_cars}</a></li>
                                        <li><a href="allBookingPageAdmin">${booking_info}</a></li>
                                        <li class="divider"></li>
                                    </ul>
                                </li>
                        </c:if>
                        <li><a href="/do/locale?lang=ru" style="padding: 0; margin-top: 7px; margin-right: -40px;"><img src="../../img/ru.ico" width="35" height="30" style="padding: 0"/></a></li>
                        <li><a href="/do/locale?lang=en" style="padding: 0; margin-top: 7px"><img src="../../img/uk.ico" width="35" height="30" style="padding: 0"/></a></li>
                        <c:if test = "${empty sessionScope.userAttributes.role}">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>${enter}</b> <span class="caret"></span></a>
                            <ul id="login-dp" class="dropdown-menu">
                                <li>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                                <div class="form-group">
                                                    <label class="sr-only" for="login">${email}</label>
                                                    <input required type="text" maxlength="20" name="email" id="login" class="form-control" placeholder="${email}">
                                                </div>
                                                <div class="form-group">
                                                    <label class="sr-only" for="exampleInputPassword2">${password}</label>
                                                    <input type="password" class="form-control" name="password" id="exampleInputPassword2" placeholder="${password}" maxlength="20" required>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-block">${submit}</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="bottom text-center">
                                            <a href="registration"><b>${registerLabel}</b></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    </c:if>
                    <c:if test = "${not empty sessionScope.userAttributes.role}">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="logout">${logout}</a></li>
                        </ul>
                            <c:if test = "${sessionScope.userAttributes.role.equals('Admin')}">
                                <%--something action for admin--%>

                            </c:if>
                        </c:if>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div><!-- /.menu-top -->
</header>

<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-md-push-3">
                <jsp:invoke fragment="content"/>
            </div>
            <div class="col-md-3 col-md-pull-9">
                <div class="sidebar">
                    <div class="col-xs-6 col-md-3">
                        <c:set var="req" value="${pageContext.request}" />
                        <c:set var="url">${req.requestURL}</c:set>
                        <c:set var="uri" value="${req.requestURI}" />
                        <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowFotoUser?index=${sessionScope.userIdAttribute}" alt="goods"></a>

                    </div>
                    <div class="row option-auto"><h3>${sessionScope.userAttributes.getFirstName()} ${sessionScope.userAttributes.getLastName()}</h3></div>
                    <div class="widget">
                        <ul>
                            <li><a href="mycar">${my_car}</a></li>
                            <li><a href="myorders">${my_orders}</a></li>
                            <li><a href="addnewcar">${add_car}</a></li>
                        </ul>
                    </div>
                    <div class="widget">
                        <ul>
                            <li><a href="mybookingcar">${my_reservations}</a></li>
                            <li><a href="setinsuser">${settings}</a></li>
                            <li><a href="logout">${logout}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
        </div>
</section>
<footer>
    <div class="footer-menu">
        <div class="container footer-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <h5>${help_info}</h5>
                            <ul>
                                <li><a href="#">${about_us}</a></li>
                                <li><a href="#">${contacts}</a></li>
                                <li><a href="#">${franchise}</a></li>
                                <li><a href="#">${agreements_and_rules}</a></li>
                                <li><a href="#">${contract_template}</a></li>
                                <li><a href="#">${blog}</a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h5>${subscribe_to_news}</h5>
                            <ul>
                                <li><a href="#"></a></li>

                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h5>${contacts}</h5>
                            <ul>
                                <li><h6>${customer_support}</h6></li>
                                <li><h3>8-800-770-7355</h3></li>
                                <li><h4>rent@auto.rent</h4></li>
                                <li><h5>WhatsApp: +7-964-341-4602</h5></li>
                                <li><h5>Viber: +7-964-341-4602</h5></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p>© 2018 CarRent. All Rights Reserved.</p>
                </div>
                <div class="col-md-7 text-right pay">

                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>
