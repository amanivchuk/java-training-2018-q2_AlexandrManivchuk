<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">
    <fmt:message key="mycar.passport" var="passport"/>
    <fmt:message key="mycar.save" var="save"/>
    <fmt:message key="mycar.passport_registration" var="passport_registration"/>
    <fmt:message key="mycar_licence_driver1" var="licence_driver1"/>
    <fmt:message key="mycar.edit" var="edit"/>
    <c:if test="${not empty successDeleteBookin}">
        <c:set value="Booking delete success" var="CsuccessDelete"/>
    </c:if>
    <c:if test="${not empty failDeleteBooking}">
        <c:set value="Booking delete fail" var="CfailDelete"/>
    </c:if>
    <c:if test="${not empty emptyMessageBooking}">
        <c:set value="Empty booking list" var="varEmptyMessageCar"/>
    </c:if>
</fmt:bundle>

<t:generiusercpage>
    <jsp:attribute name="content">
        <div class="container">
            <p>
                <strong>Мои бронирования</strong>
            </p>

            <div class="row">
                    <div class="col-md-3">
                        <form method="post" action="fotoDocumentUpload" enctype="multipart/form-data">
                            <div class="col-xs-6 col-md-3">
                                <c:set var="req" value="${pageContext.request}" />
                                <c:set var="url">${req.requestURL}</c:set>
                                <c:set var="uri" value="${req.requestURI}" />
                                <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowFotoUserDocument?index=1" alt="goods"></a>
                            </div>
                            <div class="md-form">
                                <label for="passport1">${passport}</label>
                                <input type="file" class="form-control-file" id="passport1" name="passport1">
                            </div>
                            <div class="text-center py-4 mt-3">
                                <button class="btn btn-cyan" type="submit">${save}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form method="post" action="fotoDocumentUpload" enctype="multipart/form-data">
                            <div class="col-xs-6 col-md-3">
                                <c:set var="req" value="${pageContext.request}" />
                                <c:set var="url">${req.requestURL}</c:set>
                                <c:set var="uri" value="${req.requestURI}" />
                                <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowFotoUserDocument?index=2" alt="goods"></a>
                            </div>
                            <div class="md-form">
                                <label for="passport2">${passport_registration}</label>
                                <input type="file" class="form-control-file" id="passport2" name="passport2">
                            </div>
                            <div class="text-center py-4 mt-3">
                                <button class="btn btn-cyan" type="submit">${save}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form method="post" action="fotoDocumentUpload" enctype="multipart/form-data">
                            <div class="col-xs-6 col-md-3">
                                <c:set var="req" value="${pageContext.request}" />
                                <c:set var="url">${req.requestURL}</c:set>
                                <c:set var="uri" value="${req.requestURI}" />
                                <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowFotoUserDocument?index=3" alt="goods"></a>
                            </div>
                            <div class="md-form">
                                <label for="vu1">${licence_driver1}</label>
                                <input type="file" class="form-control-file" id="vu1" name="vu1">
                            </div>
                            <div class="text-center py-4 mt-3">
                                <button class="btn btn-cyan" type="submit">${save}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form method="post" action="fotoDocumentUpload" enctype="multipart/form-data">
                            <div class="col-xs-6 col-md-3">
                                <c:set var="req" value="${pageContext.request}" />
                                <c:set var="url">${req.requestURL}</c:set>
                                <c:set var="uri" value="${req.requestURI}" />
                                <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowFotoUserDocument?index=4" alt="goods"></a>
                            </div>
                            <div class="md-form">
                                <label for="vu2">${licence_driver1}</label>
                                <input type="file" class="form-control-file" id="vu2" name="vu2">
                            </div>
                            <div class="text-center py-4 mt-3">
                                <button class="btn btn-cyan" type="submit">${save}</button>
                            </div>
                        </form>
                    </div>
                </div>

            <h3>${varEmptyMessageOrder}</h3>
            <div class="content">
                        <c:forEach var="car" varStatus="loop" items="${userListCar}">
                        <form action="booking" method="post">
                            <div class="product">
                                <div class="products-row">
                                    <div class="col-sm-4 product-img-desc">
                                        <div class="product-img">
                                            <c:set var="req" value="${pageContext.request}" />
                                            <c:set var="url">${req.requestURL}</c:set>
                                            <c:set var="uri" value="${req.requestURI}" />
                                            <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowImage?index=${loop.index}" alt="goods"></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="product-description">
                                            <input type="hidden" name="car" value="${car.id}"/>
                                            <p class="product-desc">${car.getCarcase().getCarcase()}.</p>
                                            <p class="product-title">${car.mark}  </p>
                                            <p class="product-price">${car.price} $/час</p>
                                            <div class="product-option">
                                                <ul>
                                                    <li>${car.getTransmission().getTransmission()}</li>
                                                    <li>${car.getFuel().getFuel()} </li>
                                                </ul>
                                            </div>
                                            <input type="submit" name="btn" value="${edit}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </c:forEach>
            </div>
            </table>
        </div>
    </jsp:attribute>
</t:generiusercpage>
