<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<fmt:bundle basename="i18n">
    <fmt:message key="content.city" var="city"/>
    <fmt:message key="checkout.receiving" var="receiving"/>
    <fmt:message key="checkout.ending" var="ending"/>
    <fmt:message key="content.change_city_date" var="change_city_date"/>
    <fmt:message key="content.without_driver" var="without_driver"/>
    <fmt:message key="content.with_driver" var="with_driver"/>
    <fmt:message key="content.by_name" var="by_name"/>
    <fmt:message key="content.by_price" var="by_price"/>
    <fmt:message key="content.booking" var="booking"/>
    <fmt:message key="content.car_specifications" var="car_specifications"/>
    <fmt:message key="content.fuel" var="fuel"/>
    <fmt:message key="content.petrol" var="petrol"/>
    <fmt:message key="content.diesel" var="diesel"/>
    <fmt:message key="content.gas" var="gas"/>
    <fmt:message key="content.skip_it" var="skip_it"/>
    <fmt:message key="content.transmission" var="transmission"/>
    <fmt:message key="content.manual" var="manual"/>
    <fmt:message key="content.automatic" var="automatic"/>
    <fmt:message key="content.variator" var="variator"/>
    <fmt:message key="content.suv" var="suv"/>
    <fmt:message key="content.cabriolet" var="cabriolet"/>
    <fmt:message key="content.coupe" var="coupe"/>
    <fmt:message key="content.limousin" var="limousin"/>
    <fmt:message key="content.minibus" var="minibus"/>
    <fmt:message key="content.minivan" var="minivan"/>
    <fmt:message key="content.pickup" var="pickup"/>
    <fmt:message key="content.sedan" var="sedan"/>
    <fmt:message key="content.wagon" var="wagon"/>
    <fmt:message key="content.van" var="van"/>
    <fmt:message key="content.hatchback" var="hatchback"/>
    <fmt:message key="content.body_type" var="body_type"/>

</fmt:bundle>

<t:genericpage>
    <jsp:attribute name="content">
        <div class="vc_row_header_2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 header-2-col-md-12">
                        <div class="container">
                            <div class="row">
                                <div class="info-rent">
                                    <div class="col-md-3 info-rent-city">
                                        <p>${city}</p>
                                        <div class="info-rent__city-name">
                                            ${sessionScope.userChoose.city}
                                        </div>
                                    </div>
                                    <div class="col-md-3 info-rent-date_start">
                                        <p>${receiving}</p>
                                        <div class="info-rent__date_start_info">
                                                ${sessionScope.userChoose.dateFrom}, ${sessionScope.userChoose.timeFrom}
                                        </div>
                                    </div>
                                    <div class="col-md-3 info-rent-date_end">
                                        <p>${ending}</p>
                                        <div class="info-rent__date_start_info">
                                                ${sessionScope.userChoose.dateTo}, ${sessionScope.userChoose.timeTo}
                                        </div>
                                    </div>
                                    <div class="col-md-3 info-rent-change">
                                        <div class="info-rent__change">
                                            <a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out"><span class="glyphicon glyphicon-pencil"></span>${change_city_date}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="info-rent__filter">
                                    <ul>
                                        <li>
                                            <%--<button type="button" class="btn btn-primary btn-xs">Без водителя</button>--%>
                                                <a href="<c:url value="/do/sort-by?sort-order=withDriver"/>">${without_driver}</a>
                                        </li>
                                        <li>
                                            <%--<button type="button" class="btn btn-default btn-xs">С водителелем</button>--%>
                                                <a href="<c:url value="/do/sort-by?sort-order=withoutDriver"/>">${with_driver}</a>
                                        </li>
                                        <%--<li>
                                            <button type="button" class="btn btn-default btn-xs">Подача в аеропорт</button>
                                        </li>--%>
                                        <li>
                                            <a href="<c:url value="/do/change-by?sort-order=byName"/>">${by_name}</a>
                                        </li>
                                        <li>
                                            <a href="<c:url value="/do/change-by?sort-order=byPrice"/>">${by_price}</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="range-selector">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="main-content">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-md-push-3">
                    <div class="content">
                        <c:forEach var="car" varStatus="loop" items="${listCar}">
                        <form action="booking" method="post">
                            <div class="product">
                                <div class="products-row">
                                    <div class="col-sm-4 product-img-desc">
                                        <div class="product-img">
                                            <c:set var="req" value="${pageContext.request}" />
                                            <c:set var="url">${req.requestURL}</c:set>
                                            <c:set var="uri" value="${req.requestURI}" />
                                            <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowImage?index=${loop.index}" alt="goods"></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="product-description">
                                            <input type="hidden" name="car" value="${car.id}"/>
                                                <%--<input type="hidden" name="getMark" value="<%=car.getMark()%>"/>--%>
                                            <p class="product-desc">${car.getCarcase().getCarcase()}. <%--Залог ${car.price + 800}$--%> </p>
                                            <p class="product-title">${car.mark}  </p>
                                            <p class="product-price">${car.price} $/час</p>
                                            <div class="product-option">
                                                <ul>
                                                    <li>${car.getTransmission().getTransmission()}</li>
                                                    <li>${car.getFuel().getFuel()} </li>
                                                </ul>
                                            </div>
                                            <input type="submit" name="btn" value="${booking}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </c:forEach>
                    </div>
                </div>
                <div class="col-md-3 col-md-pull-9">
                    <div class="sidebar">
                        <div class="row option-auto"><h3>${car_specifications}</h3></div>
                        <div class="widget">
                            <h3>${fuel}</h3>
                            <ul>
                                <li><a href="<c:url value="/do/change-order?sort-order=byPetrol"/> ">${petrol}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byDiesel"/>">${diesel}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byGaz"/>">${gas}</a></li>
                                <li class="more"><a href="<c:url value="/do/change-order?sort-order=noMatter"/>">${skip_it}</a></li>
                            </ul>
                        </div>
                        <div class="widget">
                            <h3>${transmission}</h3>
                            <ul>
                                <li><a href="<c:url value="/do/change-order?sort-order=byHandKPP"/>">${manual}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byAutoKPP"/>">${automatic}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byVariatorKPP"/>">${variator}</a></li>
                                <li class="more"><a href="<c:url value="/do/change-order?sort-order=noMatter"/>">${skip_it}</a></li>
                            </ul>
                        </div>
                        <div class="widget">
                            <h3>${body_type}</h3>
                            <ul>
                                <li><a href="<c:url value="/do/change-order?sort-order=byVnedoroznik"/>">${suv}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byKabriolet"/>">${cabriolet}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byKupe"/>">${coupe}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byLimuzin"/>">${limousin}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byMikroavtobus"/>">${minibus}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byMiniven"/>">${minivan}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byPikap"/>">${pickup}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=bySedan"/>">${sedan}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byUniversal"/>">${wagon}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byFurgon"/>">${van}</a></li>
                                <li><a href="<c:url value="/do/change-order?sort-order=byHechback"/>">${hatchback}</a></li>
                                <li class="more"><a href="<c:url value="/do/change-order?sort-order=noMatter"/>">${skip_it}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </jsp:attribute>
</t:genericpage>
