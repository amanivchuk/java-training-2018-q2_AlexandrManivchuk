<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">


    <c:if test="${not empty successDeleteBooking}">
        <c:set value="User delete success" var="BsuccessDelete"/>
    </c:if>
    <c:if test="${not empty failDeleteBooking}">
        <c:set value="User delete fail" var="BfailDelete"/>
    </c:if>
    <c:if test="${not empty emptyMessageBooking}">
        <c:set value="Empty user list" var="varEmptyMessageBooking"/>
    </c:if>
</fmt:bundle>

<t:genericpage>
    <jsp:attribute name="content">

        <div class="global-wrap container-fluid">
            <div class="row">
                <div class="container">

                    <span class="label-success">${BsuccessDelete}</span>
                    <span class="label-warning">${BfailDelete}</span>
                    <c:remove var="successDeleteBooking" scope="session"/>
                    <c:remove var="failDeleteBooking" scope="session"/>
                    <p>
                        <strong>Information about all booking cars</strong>
                    <div class="col-xs-6 col-md-3">
                        <c:set var="req" value="${pageContext.request}" />
                        <c:set var="url">${req.requestURL}</c:set>
                        <c:set var="uri" value="${req.requestURI}" />
                    </div>
                    </p>
                    ${varEmptyMessageBooking}
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Last name</th>
                            <th>Phone</th>
                            <th>Auto</th>
                            <%--<th>Status</th>--%>
                            <th>Time from</th>
                            <th>Time to</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${bookingList}" var="booking">
                        <tr>
                            <td><c:out value="${booking.getUser().getFirstName()}"/></td>
                            <td><c:out value="${booking.getUser().getLastName()}"/></td>
                            <td><c:out value="${booking.getUser().getPhone()}"/></td>
                            <td><c:out value="${booking.getCar().getMark()}"/></td>
                            <%--<td><c:out value="${booking.getCar().isAvailability()}"/></td>--%>
                            <td><c:out value="${booking.getFrom()}"/></td>
                            <td><c:out value="${booking.getTo()}"/></td>
                            <td><c:out value="${booking.totalPrice}"/></td>
                            <td><a href="<c:url value="/do/delete-booking?id=${booking.id}"/>">Закрыть заказ</a></td>
                            <td><a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample${booking.getCar().getId()}" aria-expanded="false" aria-controls="collapseExample">Подробнее</a></td>
                        </tr>
                            <tr>
                                <div class="collapse" id="collapseExample${booking.getCar().getId()}">
                                    <div class="well">
                                        <p>Passport information</p>
                                        <div class="col-xs-6 col-md-3">
                                            <c:set var="req" value="${pageContext.request}" />
                                            <c:set var="url">${req.requestURL}</c:set>
                                            <c:set var="uri" value="${req.requestURI}" />
                                            <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowDocument?user_id=${booking.getUser().getId()}&index=1" alt="goods"></a>
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <c:set var="req" value="${pageContext.request}" />
                                            <c:set var="url">${req.requestURL}</c:set>
                                            <c:set var="uri" value="${req.requestURI}" />
                                            <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowDocument?user_id=${booking.getUser().getId()}&index=2" alt="goods"></a>
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <c:set var="req" value="${pageContext.request}" />
                                            <c:set var="url">${req.requestURL}</c:set>
                                            <c:set var="uri" value="${req.requestURI}" />
                                            <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowDocument?user_id=${booking.getUser().getId()}&index=3" alt="goods"></a>
                                        </div>
                                        <div class="col-xs-6 col-md-3">
                                            <c:set var="req" value="${pageContext.request}" />
                                            <c:set var="url">${req.requestURL}</c:set>
                                            <c:set var="uri" value="${req.requestURI}" />
                                            <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowDocument?user_id=${booking.getUser().getId()}&index=4" alt="goods"></a>
                                        </div>
                                        <br/>
                                        <form method="post" action="rejectBooking">
                                            <input hidden name="id" value="${booking.id}"/>
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Example textarea</label>
                                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="comment"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary mb-2">Отменить заказ</button>
                                        </form>
                                    </div>
                                </div>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            </jsp:attribute>
</t:genericpage>