<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">


    <c:if test="${not empty successDeleteCar}">
        <c:set value="User delete success" var="CsuccessDelete"/>
    </c:if>
    <c:if test="${not empty failDeleteCar}">
        <c:set value="User delete fail" var="CfailDelete"/>
    </c:if>
    <c:if test="${not empty emptyMessageCar}">
        <c:set value="Empty user list" var="varEmptyMessageCar"/>
    </c:if>
</fmt:bundle>

<t:genericpage>
    <jsp:attribute name="content">

        <div class="global-wrap container-fluid">
            <div class="row">
                <div class="container">

                    <span class="label-success">${CsuccessDelete}</span>
                    <span class="label-warning">${CfailDelete}</span>
                    <c:remove var="successDeleteCar" scope="session"/>
                    <c:remove var="failDeleteCar" scope="session"/>
                    <p>
                        <strong>Information about all register users</strong>
                    </p>
                    ${varEmptyMessageCar}
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>Mark</th>
                            <th>Year</th>
                            <th>Price</th>
                            <th>Booked</th>
                            <th>City</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${carsList}" var="car">
                        <tr>
                            <td><c:out value="${car.mark}"/></td>
                            <td><c:out value="${car.year}"/></td>
                            <td><c:out value="${car.price}"/></td>
                            <td><c:out value="${car.availability}"/></td>
                            <td><c:out value="${car.getCity().getCity()}"/></td>
                            <td><a href="<c:url value="/do/delete-car?id=${car.id}"/>">Delete car</a></td>
                        </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            </jsp:attribute>
</t:genericpage>