<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<fmt:bundle basename="i18n">
    <fmt:message key="loginpage.enter" var="enter"/>
    <fmt:message key="registration.label.register" var="register"/>
    <fmt:message key="registration.label.password" var="password"/>
    <fmt:message key="registration.label.enter" var="enter"/>
</fmt:bundle>

<t:genericpage>
    <jsp:attribute name="login">
       <div class="container">
           <div class="row" style="margin-top: 150px;">

               <div class="col-md-offset-3 col-md-6">
                   <form class="form-horizontal" method="post" action="login">
                <span class="heading">${enter}
                <a href="registration" class="glyphicon glyphicon-share">${register}</a>
                </span>
                       <div class="form-group">
                           <input type="email" class="form-control" name="email" id="inputEmail" placeholder="E-mail">
                           <i class="fa fa-user"></i>
                       </div>
                       <div class="form-group help">
                           <input type="password" class="form-control" name="password" id="inputPassword" placeholder="${password}">
                           <i class="fa fa-lock"></i>
                           <a href="#" class="fa fa-question-circle"></a>
                       </div>
                       <div class="form-group">
                           <%--<div class="main-checkbox">
                               <input type="checkbox" value="none" id="checkbox1" name="check"/>
                               <label for="checkbox1"></label>
                           </div>
                           <span class="text">Запомнить</span>--%>
                           <button type="submit" class="btn btn-default">${enter}</button>
                       </div>
                   </form>
               </div>

           </div><!-- /.row -->
       </div><!-- /.container -->
    </jsp:attribute>
</t:genericpage>
