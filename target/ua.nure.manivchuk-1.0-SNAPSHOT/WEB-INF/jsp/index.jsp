<%@ page contentType="text/html;charset=utf-8" %>
<%@ include file="import.jspf" %>
<html>
<head>
    <%@ include file="head.jspf" %>
    <title>Rent Car</title>
</head>
<body>
    <%@include file="header.jspf" %>

    <%@include file="finder.jspf" %>

    <footer>
        <div class="footer-menu">
            <div class="container footer-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <h5>Help &amp; Info</h5>
                                <ul>
                                    <li><a href="#">О нас</a></li>
                                    <li><a href="#">Контакты</a></li>
                                    <li><a href="#">Франшиза</a></li>
                                    <li><a href="#">Соглашение и правила</a></li>
                                    <li><a href="#">Шаблон договора</a></li>
                                    <li><a href="#">Все города</a></li>
                                    <li><a href="#">Блог</a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h5>Подписаться на новости сервиса</h5>
                                <ul>
                                    <li><a href="#"></a></li>

                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h5>Care &amp; advice</h5>
                                <ul>
                                    <li><h6>24/7 поддержка клиентов
                                        (звонок бесплатный)</h6></li>
                                    <li><h3>8-800-770-7355</h3></li>
                                    <li><h4>rent@auto.rent</h4></li>
                                    <li><h5>WhatsApp: +7-964-341-4602</h5></a></li>
                                    <li><h5>Viber: +7-964-341-4602</h5></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <p>© 2014 Fashion Store. All Rights Reserved.</p>
                    </div>
                    <div class="col-md-7 text-right pay">

                    </div>
                </div>
            </div>
        </div>
    </footer>

</body>
</html>
