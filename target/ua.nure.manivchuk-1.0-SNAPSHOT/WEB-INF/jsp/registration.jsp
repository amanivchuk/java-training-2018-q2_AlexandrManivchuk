<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Регистрация AUTO.rent</title>

    <link href="../../css/autorization.css" rel="stylesheet">
</head>
<body>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:bundle basename="i18n">
    <fmt:message key="loginpage.enter" var="enter"/>
    <fmt:message key="registration.label.register" var="register"/>
    <fmt:message key="registration.label.password" var="password"/>
    <fmt:message key="checkout.name" var="name"/>
    <fmt:message key="checkout.surname" var="surname"/>
    <fmt:message key="registration.label.enter" var="enter"/>
    <fmt:message key="registration.label.conPass" var="conPass"/>
    <fmt:message key="registration.registration_do" var="registration"/>

    <c:if test="${not empty firstNameEmpty}">
        <fmt:message key="registration.error.firstNameEmpty" var="varFirstNameEmpty"/></c:if>
    <c:if test="${not empty invalidFirstName}">
        <fmt:message key="registration.error.invalidFirstName" var="varInvalidFirstName"/></c:if>
    <c:if test="${not empty lastNameEmpty}">
        <fmt:message key="registration.error.lastNameEmpty" var="varLastNameEmpty"/></c:if>
    <c:if test="${not empty invalidLastName}">
        <fmt:message key="registration.error.invalidLastName" var="varInvalidLastName"/></c:if>
    <c:if test="${not empty emailEmpty}">
        <fmt:message key="registration.error.emailEmpty" var="varEmailEmpty"/></c:if>
    <c:if test="${not empty invalidEmail}">
        <fmt:message key="registration.error.invalidEmail" var="varInvalidEmail"/></c:if>
    <c:if test="${not empty busyEmail}">
        <fmt:message key="registration.error.busyEmail" var="varBusyEmail"/></c:if>
    <c:if test="${not empty passwordIsEmpty}">
        <fmt:message key="registration.error.passwordIsEmpty" var="varPasswordIsEmpty"/></c:if>
    <c:if test="${not empty invalidPassword}">
        <fmt:message key="registration.error.invalidPassword" var="varInvalidPassword"/></c:if>
    <c:if test="${not empty confirmError}">
        <fmt:message key="registration.error.confirmError" var="varConfirmError"/></c:if>
</fmt:bundle>

<div class="container">
    <div class="row" style="margin-top: 150px;">

        <div class="col-md-offset-3 col-md-6">
            <form class="form-horizontal" method="post" action="register">
                 <span class="heading">${register}
               <%-- <a href="autorization" class="glyphicon glyphicon-share" style=" padding-left: 220px;">Вход на сайт</a>--%>
                </span>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" id="name" placeholder="${name}">
                    <i class="fa fa-user"></i>
                    <span class="error"> ${varFirstNameEmpty}<br/>${varInvalidFirstName}<br/></span>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="lastname" id="surname" placeholder="${surname}">
                    <i class="fa fa-user"></i>
                   <span class="error"> ${varLastNameEmpty}<br/>${varInvalidLastName}<br/></span>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="inputEmail" name="email" placeholder="E-mail">
                    <i class="fa fa-user"></i>
                    <span class="error"> ${varEmailEmpty}<br/>${varInvalidEmail}${varBusyEmail}<br/></span>
                </div>
                <div class="form-group help">
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="${password}">
                    <i class="fa fa-lock"></i>
                    <span class="error"> ${varPasswordIsEmpty}<br/>${varInvalidPassword}<br/></span>
                </div>
                <div class="form-group help">
                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="${conPass}">
                    <i class="fa fa-lock"></i>
                    <span class="error"> ${varConfirmError}</span>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">${registration}</button>
                </div>
            </form>
        </div>

    </div><!-- /.row -->
</div><!-- /.container -->
</body>
</html>