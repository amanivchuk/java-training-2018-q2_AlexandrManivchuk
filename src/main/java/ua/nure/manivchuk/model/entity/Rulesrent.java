package ua.nure.manivchuk.model.entity;

import java.util.Objects;

/**
 * Created by Lenovo on 5/2/2018.
 */
public class Rulesrent extends BaseEntity{
    private String rulerent;

    public String getRulerent() {
        return rulerent;
    }

    public void setRulerent(String rulerent) {
        this.rulerent = rulerent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Rulesrent rulesrent = (Rulesrent) o;
        return Objects.equals(rulerent, rulesrent.rulerent);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), rulerent);
    }

    @Override
    public String toString() {
        return "Rulesrent{" +
                "rulerent='" + rulerent + '\'' +
                '}';
    }
}
