package ua.nure.manivchuk.model.entity;

import java.util.Objects;

/**
 * Created by Lenovo on 5/2/2018.
 */
public class Fuel extends BaseEntity{
    private String fuel;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Fuel fuel1 = (Fuel) o;
        return Objects.equals(fuel, fuel1.fuel);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), fuel);
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    @Override
    public String toString() {
        return "Fuel{" +
                "id=" + getId() +
                ", fuel='" + fuel + '\'' +
                '}';
    }
}
