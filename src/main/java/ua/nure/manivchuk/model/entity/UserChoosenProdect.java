package ua.nure.manivchuk.model.entity;

public class UserChoosenProdect {
    private Car car;
    private String city;
    private String dateFrom;
    private String timeFrom;
    private String dateTo;
    private String timeTo;
    private long price;

    public UserChoosenProdect() {
    }


    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "UserChoosenProdect{" +
                "choosenCar=" + car +
                ", city='" + city + '\'' +
                ", dateFrom='" + dateFrom + '\'' +
                ", timeFrom='" + timeFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", timeTo='" + timeTo + '\'' +
                '}';
    }
}
