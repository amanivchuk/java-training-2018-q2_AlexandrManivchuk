package ua.nure.manivchuk.model.entity;

import java.util.Objects;

/**
 * Created by Lenovo on 5/2/2018.
 */
public class Classific extends BaseEntity{
    private String classific;

    public String getClassific() {
        return classific;
    }

    public void setClassific(String classific) {
        this.classific = classific;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Classific classific1 = (Classific) o;
        return Objects.equals(classific, classific1.classific);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), classific);
    }
}
