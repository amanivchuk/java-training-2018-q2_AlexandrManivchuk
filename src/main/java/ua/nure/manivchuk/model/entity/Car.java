package ua.nure.manivchuk.model.entity;


import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Lenovo on 5/2/2018.
 */
public class Car extends BaseEntity{
    private String mark;
    private String issuecontry;
    private int year;
    private int price;
    private boolean availability;
    private Classific classific;
    private Carcase carcase;
    private Fuel fuel;
    private ArrayList<Rulesrent> rulesrent;
    private Transmission transmission;
    private City city;
    private byte[] image;
    private ArrayList<Photos> photos;

    @Override
    public String toString() {
        return "Car{ "+ super.getId() +
                "mark='" + mark + '\'' +
                ", issuecontry='" + issuecontry + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", availability=" + availability +
                ", classific=" + classific +
                ", carcase=" + carcase +
                ", fuel=" + fuel +
                ", rulesrent=" + rulesrent +
                ", transmission=" + transmission +
                ", city=" + city +
                '}';
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getIssuecontry() {
        return issuecontry;
    }

    public void setIssuecontry(String issuecontry) {
        this.issuecontry = issuecontry;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public Classific getClassific() {
        return classific;
    }

    public void setClassific(Classific classific) {
        this.classific = classific;
    }

    public Carcase getCarcase() {
        return carcase;
    }

    public void setCarcase(Carcase carcase) {
        this.carcase = carcase;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public ArrayList<Rulesrent> getRulesrent() {
        return rulesrent;
    }

    public void setRulesrent(ArrayList<Rulesrent> rulesrent) {
        this.rulesrent = rulesrent;
    }

    public Transmission getTransmission() {
        return transmission;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public ArrayList<Photos> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<Photos> photos) {
        this.photos = photos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Car car = (Car) o;
        return year == car.year &&
                price == car.price &&
                availability == car.availability &&
                Objects.equals(mark, car.mark) &&
                Objects.equals(issuecontry, car.issuecontry) &&
                Objects.equals(classific, car.classific) &&
                Objects.equals(carcase, car.carcase) &&
                Objects.equals(fuel, car.fuel) &&
                Objects.equals(rulesrent, car.rulesrent) &&
                Objects.equals(transmission, car.transmission) &&
                Objects.equals(city, car.city);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), mark, issuecontry, year, price, availability, classific, carcase, fuel, rulesrent, transmission, city);
    }
}