package ua.nure.manivchuk.model.entity;

import java.util.Objects;

/**
 * Created by Lenovo on 5/2/2018.
 */
public class Transmission extends BaseEntity{
    private String transmission;

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Transmission that = (Transmission) o;
        return Objects.equals(transmission, that.transmission);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), transmission);
    }

    @Override
    public String toString() {
        return "Transmission{" +
                "transmission='" + transmission + '\'' +
                '}';
    }
}
