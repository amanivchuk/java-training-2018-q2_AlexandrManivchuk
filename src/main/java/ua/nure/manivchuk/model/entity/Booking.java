package ua.nure.manivchuk.model.entity;

import java.sql.Timestamp;

public class Booking extends BaseEntity {
    private User user;
    private Car car;
    private Timestamp from;
    private Timestamp to;
    private int totalPrice;

    public Booking() {
    }

    public Booking(User user, Car car, Timestamp from, Timestamp to) {
        this.user = user;
        this.car = car;
        this.from = from;
        this.to = to;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Timestamp getFrom() {
        return from;
    }

    public void setFrom(Timestamp from) {
        this.from = from;
    }

    public Timestamp getTo() {
        return to;
    }

    public void setTo(Timestamp to) {
        this.to = to;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Booking{ " + super.getId() +
                ", user=" + user +
                ", car=" + car +
                ", from=" + from +
                ", to=" + to +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
