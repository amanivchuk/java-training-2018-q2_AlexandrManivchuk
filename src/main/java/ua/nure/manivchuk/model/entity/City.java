package ua.nure.manivchuk.model.entity;

/**
 * Created by Lenovo on 5/5/2018.
 */
public class City extends BaseEntity{
    private String city;
    private String country;
    private String data;

    public City() {
    }

    public City( String city, String country, String data) {
        this.city = city;
        this.country = country;
        this.data = data;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "City{" +
                "city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
