package ua.nure.manivchuk.model.entity;

import java.util.Arrays;

public class User extends BaseEntity{
    private boolean status;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private String role;
    private byte[] foto;
    private byte[] document1;
    private byte[] document2;
    private byte[] document3;
    private byte[] document4;

    public User() {
    }

    public User(boolean status, String firstName, String lastName, String email, String password, String phone, String role) {
        this.status = status;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.role = role;
    }

    public User(String firstName, String lastName, String email, String password, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public User(String firstName, String lastName, String email, String password, String phone, String role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.role = role;
    }

    public User(String firstName, String lastName, String email, String password, String phone, String role, byte[] foto) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.role = role;
        this.foto = foto;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public byte[] getDocument1() {
        return document1;
    }

    public void setDocument1(byte[] document1) {
        this.document1 = document1;
    }

    public byte[] getDocument2() {
        return document2;
    }

    public void setDocument2(byte[] document2) {
        this.document2 = document2;
    }

    public byte[] getDocument3() {
        return document3;
    }

    public void setDocument3(byte[] document3) {
        this.document3 = document3;
    }

    public byte[] getDocument4() {
        return document4;
    }

    public void setDocument4(byte[] document4) {
        this.document4 = document4;
    }

    public boolean isStatus(boolean status) {
        return this.status;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" +
                "status=" + status +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
