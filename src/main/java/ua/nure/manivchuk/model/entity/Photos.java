package ua.nure.manivchuk.model.entity;

import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Lenovo on 5/10/2018.
 */
public class Photos extends BaseEntity{
    private byte[] image;
    private int cars_id;


    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getCars_id() {
        return cars_id;
    }

    public void setCars_id(int cars_id) {
        this.cars_id = cars_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Photos photos = (Photos) o;
        return cars_id == photos.cars_id &&
                Arrays.equals(image, photos.image);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(super.hashCode(), cars_id);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }

    @Override
    public String toString() {
        return "Photos{" +
                "id=" + getId() +
                ", cars_id=" + cars_id +
                '}';
    }
}
