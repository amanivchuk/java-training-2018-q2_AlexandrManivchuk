package ua.nure.manivchuk.model.entity;

import java.util.Objects;

/**
 * Created by Lenovo on 5/2/2018.
 */
public class Carcase extends BaseEntity{
    private String carcase;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Carcase carcase1 = (Carcase) o;
        return Objects.equals(carcase, carcase1.carcase);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), carcase);
    }

    public String getCarcase() {
        return carcase;
    }

    public void setCarcase(String carcase) {
        this.carcase = carcase;
    }
}
