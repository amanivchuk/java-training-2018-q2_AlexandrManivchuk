package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Classific;
import ua.nure.manivchuk.model.entity.Rulesrent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class RulesrentDAOJdbc extends AbstractDAOJdbc<Classific> {
    private static final Logger LOGGER = LoggerFactory.getLogger(RulesrentDAOJdbc.class);

    public RulesrentDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public ArrayList<Rulesrent> getAllRulesrent() throws DAOException {
        ArrayList<Rulesrent> rulesrents = new ArrayList<>();
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("SELECT_RULESRENT_LIST");
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Rulesrent rulesrent = new Rulesrent();
                rulesrent.setId(resultSet.getInt("rulerent_id"));
                rulesrent.setRulerent(resultSet.getString("rulerent"));
                rulesrents.add(rulesrent);
            }
            return rulesrents;
        } catch (SQLException e) {
            LOGGER.error("Cannot get all rulesrents", e);
            throw new DAOException("Cannot get all rulesrents");
        }
    }

    public ArrayList<Rulesrent> getRulesrentByName(String[] rulesrentArray) throws DAOException {
        ArrayList<Rulesrent> rulesrents = new ArrayList<>();
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("SELECT_RELESRENT_BY_NAME");
        for(int i = 0; i < rulesrentArray.length; i++){
            String name = rulesrentArray[i];
            try{
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, name);
                ResultSet resultSet = statement.executeQuery();
                while(resultSet.next()){
                    Rulesrent rulesrent = new Rulesrent();
                    rulesrent.setId(resultSet.getInt("rulerent_id"));
                    rulesrent.setRulerent(resultSet.getString("rulerent"));
                    rulesrents.add(rulesrent);
                }
            } catch (SQLException e) {
                LOGGER.error("Cannot get rulesrents by name", e);
                throw new DAOException("Cannot get rulesrents by name");
            }
        }
        return rulesrents;
    }

    public void updateRulerentByCarId(int car_id, int rulerent_id) throws DAOException {
        try{
            String query = getQuery("CREATE_RULERENT_BY_CAR_ID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, car_id);
            statement.setInt(2, rulerent_id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Cannot update rulesrent by car_id", e);
            throw new DAOException("Cannot update rulesrent by car_id");
        }
    }
}
