package ua.nure.manivchuk.model.dao.factory;

import ua.nure.manivchuk.connection.ConnectionPoolException;
import ua.nure.manivchuk.connection.DBConnectionPool;
import ua.nure.manivchuk.model.dao.AbstractDAO;
import ua.nure.manivchuk.model.dao.jdbc.AbstractDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;

public class DaoFactoryJdbc extends AbstractDAOFactory {
    private static final DBConnectionPool connectionPool = DBConnectionPool.getInstance();
    private static final Logger LOGGER  = LoggerFactory.getLogger(DaoFactoryJdbc.class);
    private Connection connection;

    public DaoFactoryJdbc() throws DAOException {
        try{
            if (connection == null){
                connection = connectionPool.getConnection();
            }
        } catch (ConnectionPoolException e) {
            LOGGER.error("Cannot create connection", e);
            e.printStackTrace();
            throw new DAOException("Cannot initialize connection", e);
        }
    }

    @Override
    public <T extends AbstractDAO> T getDAO(Class<T> clazz) throws DAOException {
        AbstractDAOJdbc dao;
        try{
            Constructor<T> constructor = clazz.getConstructor(Connection.class);
            LOGGER.info("Connection is null - {}", connection == null);
            dao = (AbstractDAOJdbc) constructor.newInstance(connection);
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new DAOException("Cannot get dao instance", e);
        }
        return (T) dao;
    }

    public void startTransaction(){
        try{
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        } catch (SQLException e) {
            LOGGER.error("Cannot start transaction", e);
            e.printStackTrace();
        }
    }

    public void commitTransaction() throws DAOException{
        try{
            connection.commit();
            connectionPool.closeConnection();
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("Cannot commit transaction", e);
            e.printStackTrace();
            throw new DAOException("Cannot commit transaction");
        }
    }

    public void roolbackTransaction() throws DAOException{
        try{
            connection.rollback();
            connectionPool.closeConnection();
        } catch (SQLException | ConnectionPoolException e) {
            LOGGER.error("Cannot rollback connection", e);
            e.printStackTrace();
            throw new DAOException("cannot rollback transaction");
        }
    }

    public void closeConnection(){
        try{
            connectionPool.closeConnection();
        } catch (ConnectionPoolException e) {
            LOGGER.error("cannot close connection");
            e.printStackTrace();
            throw new RuntimeException("Cannot close connection");
        }
    }
}
