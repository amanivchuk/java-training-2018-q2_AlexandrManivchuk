package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Classific;
import ua.nure.manivchuk.model.entity.Fuel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FuelDAOJdbc extends AbstractDAOJdbc<Classific> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FuelDAOJdbc.class);

    public FuelDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public ArrayList<Fuel> getAllFuels() throws DAOException {
        ArrayList<Fuel> fuels = new ArrayList<>();
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("SELECT_FUEL_LIST");
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Fuel fuel = new Fuel();
                fuel.setId(resultSet.getInt("id_fuel"));
                fuel.setFuel(resultSet.getString("fuel"));
                fuels.add(fuel);
            }
            return fuels;
        } catch (SQLException e) {
            LOGGER.error("Cannot get all fuels", e);
            throw new DAOException("Cannot get all fuels");
        }
    }

    public Fuel getFuelId(String fuelName) throws DAOException {
        Fuel fuel = null;
        try {
            String query = getQuery("SELECT_FUEL_ID_BY_NAME");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, fuelName);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                fuel = new Fuel();
                fuel.setId(resultSet.getInt("id_fuel"));
            }
            return fuel;
        } catch (SQLException e) {
            LOGGER.error("Cannot get fuel id by name ", e);
            throw new DAOException("Cannot get fuel id by name");
        }
    }
}
