package ua.nure.manivchuk.model.dao.factory;

import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EntityFactory<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(EntityFactory.class);

    public T getInstanceFromResSet(ResultSet set, Class<T> aClass) throws DAOException{
        T object = null;
        try{
            object = aClass.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot create an instance of Class", e);
            e.printStackTrace();
        }
        try{
            if(object instanceof User){
                object = (T) getUser(set);
            } else if(object instanceof Classific){
                object = (T) getClassific(set);
            } else if(object instanceof Carcase){
                object = (T) getCarcase(set);
            } else if(object instanceof Fuel){
                object = (T) getFuel(set);
            } else if(object instanceof Rulesrent){
                object = (T) getRulesrent(set);
            } else if(object instanceof Transmission){
                object = (T) getTransmission(set);
            } else if(object instanceof City){
                object = (T) getCity(set);
            }
        } catch (ClassCastException e){
            throw new DAOException("Cannot get item by Id");
        }
        return object;
    }

    private City getCity(ResultSet set) throws DAOException {
        try{
            City city = new City();
            while (set.next()){
                city.setId(set.getInt("id"));
                city.setCity(set.getString("city"));
                city.setCountry(set.getString("country"));
                city.setData(set.getString("data"));
            }
            System.out.println("===> " + city);
            return city;
        } catch (SQLException e) {
            LOGGER.error("Cannot get City by id");
            throw new DAOException("Cannot get City by id");
        }
    }

    private Transmission getTransmission(ResultSet set) throws DAOException {
        try{
            Transmission transmission = new Transmission();
            while (set.next()){
                transmission.setId(set.getInt("id_transmission"));
                transmission.setTransmission(set.getString("transmission"));
            }
            return transmission;
        } catch (SQLException e) {
            LOGGER.error("Cannot get Transmission by id");
            throw new DAOException("Cannot get Transmission by id");
        }
    }

    private Rulesrent getRulesrent(ResultSet set) throws DAOException {
        try{
            Rulesrent rulesrent = new Rulesrent();
            while (set.next()){
                rulesrent.setId(set.getInt("rulerent_id"));
                rulesrent.setRulerent(set.getString("rulerent"));
            }
            LOGGER.info("Rulesrent===>" + rulesrent);
            return rulesrent;
        } catch (SQLException e) {
            LOGGER.error("Cannot get Rulerent by id");
            throw new DAOException("Cannot get Rulerent by id");
        }
    }

    private Fuel getFuel(ResultSet set) throws DAOException {
        try{
            Fuel fuel = new Fuel();
            while (set.next()){
                fuel.setId(set.getInt("id_fuel"));
                fuel.setFuel(set.getString("fuel"));
            }
            System.out.println("Fuel=>>>" + fuel);
            return fuel;
        } catch (SQLException e) {
            LOGGER.error("Cannot get Fuel by id");
            throw new DAOException("Cannot get Fuel by id");
        }
    }

    private Carcase getCarcase(ResultSet set) throws DAOException {
        try{
            Carcase carcase = new Carcase();
            while (set.next()){
                carcase.setId(set.getInt("carcase_id"));
                carcase.setCarcase(set.getString("carcase"));
            }
            return carcase;
        } catch (SQLException e) {
            LOGGER.error("Cannot get Carcase by id");
            throw new DAOException("Cannot get Carcase by id");
        }
    }

    private Classific getClassific(ResultSet set) throws DAOException {
        try{
            Classific classific = new Classific();
            while (set.next()){
                classific.setId(set.getInt("classific_id"));
                classific.setClassific(set.getString("classific"));
            }
            return classific;
        } catch (SQLException e) {
            LOGGER.error("Cannot get Classific by id");
            throw new DAOException("Cannot get Classific by id");
        }
    }

    private User getUser(ResultSet set) throws DAOException{
        try{
            User user = new User();
            while (set.next()){
                user.setId(set.getInt("id"));
                user.setStatus(set.getBoolean("status"));
                user.setEmail(set.getString("email"));
                user.setFirstName(set.getString("FirstName"));
                user.setLastName(set.getString("LastName"));
                user.setPassword(set.getString("password"));
                user.setRole(set.getString("role_name"));
                user.setPhone(set.getString("phone"));
                user.setFoto(set.getBytes("foto"));
                user.setDocument1(set.getBytes("passport1"));
                user.setDocument2(set.getBytes("passport2"));
                user.setDocument3(set.getBytes("vu1"));
                user.setDocument4(set.getBytes("vu2"));
            }
            return user;
        } catch (SQLException e) {
            LOGGER.error("Cannot get User by Id");
            e.printStackTrace();
            throw new DAOException("Cannot get User by Id");
        }
    }

}
