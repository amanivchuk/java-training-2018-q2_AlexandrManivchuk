package ua.nure.manivchuk.model.dao.jdbc;


import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.factory.EntityFactory;
import ua.nure.manivchuk.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class UserDAOJdbc extends AbstractDAOJdbc<User> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDAOJdbc.class);

    public UserDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public long create(User user) throws DAOException {
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("CREATE_USER");
        try{
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getLastName());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getEmail());
            statement.setInt(4, 2);
            statement.setString(5, user.getPassword());
            statement.setString(6, user.getPhone());
            statement.setInt(7, 0);
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            long key = -1;
            if (rs != null && rs.next()) {
                key = rs.getLong(1);
            }
            LOGGER.info("KEY==================>>>>" + key);
            return key;
        } catch (SQLException e) {
            LOGGER.error("Cannot create new user", e);
            e.printStackTrace();
            throw new DAOException("Cannot create new user");
        }
    }

    public User findByEmail(String email) throws DAOException{
        User user;
        try{
            ResourceBundle resource = ResourceBundle.getBundle("crud");
            String query = resource.getString("USER_GET_BY_EMAIL");
            EntityFactory<User> factory = new EntityFactory<>();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, email);
            ResultSet set = statement.executeQuery();
            user = factory.getInstanceFromResSet(set, User.class);
        } catch (SQLException e) {
            LOGGER.error("Dao find by email operation is failed", e);
            e.printStackTrace();
            throw new DAOException("Dao find by email operation is failed");
        }
        return user;
    }

    public List<User> getAllUser() throws DAOException {
        List<User> users = new ArrayList<>();
        String query = getQuery("USER_GET_ALL");
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstname"));
                user.setLastName(resultSet.getString("lastname"));
                user.setEmail(resultSet.getString("email"));
                user.setRole(resultSet.getString("role_name"));
                user.setPhone(resultSet.getString("phone"));
                user.setFoto(resultSet.getBytes("foto"));
                user.setStatus(resultSet.getBoolean("status"));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            LOGGER.error("Cannot Get All Users", e);
            e.printStackTrace();
            throw new DAOException("Cannot Get All Users");
        }
    }
    public static void main(String[] args) throws DAOException {

        DaoFactoryJdbc factory = new DaoFactoryJdbc();
        UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
        List<User> lit = daoJdbc.getAllUser();
        for(User u : lit){
            System.out.println(u);
        }
    }

    public void delete(User user) throws DAOException {
        LOGGER.info("User for delete: " + user.toString());
        try{
            ResourceBundle resource = ResourceBundle.getBundle("crud");
            String query = resource.getString("DELETE_USER");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, user.getId());
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Dao delete operation is failed", e);
            throw new DAOException("Dao delete operation is failed");
        }
    }

    public void updateCarUser(int user_id, int car_id) throws DAOException {
        try{
            String query = getQuery("UPDATE_CAR_ADDED_TO_USER");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, user_id);
            statement.setInt(2, car_id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Cannot update users added car", e);
            throw new DAOException("Cannot update users added car");
        }
    }

    public void updateUser(User user) throws DAOException {
        try{
            String query = getQuery("UPDATE_USER");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPhone());
            statement.setInt(5, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Cannot update user", e);
            throw new DAOException("Cannot update user");
        }
    }

    public void updateUserWithPhoto(User user, InputStream file) throws DAOException {
        try{
            String query = getQuery("UPDATE_USER_WITH_PHOTO");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPhone());
            statement.setBlob(5, file);
            statement.setInt(6, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Cannot update user with photo", e);
            throw new DAOException("Cannot update user with photo");
        }
    }

    public User getUserById(int id) throws DAOException {
        User user = null;
        try{
            String query = getQuery("USER_GET_BY_ID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setFirstName(resultSet.getString("firstname"));
                user.setLastName(resultSet.getString("lastname"));
                user.setEmail(resultSet.getString("email"));
                user.setRole(resultSet.getString("role_name"));
                user.setPhone(resultSet.getString("phone"));
                user.setFoto(resultSet.getBytes("foto"));
                user.setStatus(resultSet.getBoolean("status"));
            }
            return user;
        } catch (SQLException e) {
            LOGGER.error("Cannot Get user", e);
            e.printStackTrace();
            throw new DAOException("Cannot Get user");
        }
    }

    public void updatePasswordUser(String newpassword, int user_id) throws DAOException {
        try{
            String query = getQuery("UPDATE_PASSWORD_USER");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, newpassword);
            statement.setInt(2, user_id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Cannot update user password", e);
            throw new DAOException("Cannot update user password");
        }
    }

    public void updateUserFotoDocument(InputStream file, int user_id, String attributeName) throws DAOException {
        try{
            String query = getQuery("UPDATE_USER_PHOTO_DOCUMENT_"+attributeName);
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setBlob(1, file);
//            statement.setString(2, attributeName);
            statement.setInt(2, user_id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Cannot update user password", e);
            throw new DAOException("Cannot update user password");
        }
    }

    public void block(User user, int statusBlock) throws DAOException {
        LOGGER.info("User for block: " + user.toString());
        try{
            ResourceBundle resource = ResourceBundle.getBundle("crud");
            String query = resource.getString("UPDATE_BLOCK_USER");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, statusBlock);
            statement.setInt(2, user.getId());
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Dao delete operation is failed", e);
            throw new DAOException("Dao delete operation is failed");
        }
    }
}
