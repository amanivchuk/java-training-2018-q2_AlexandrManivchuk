package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.AbstractDAOFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.entity.Booking;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.City;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.ServiceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class BookingDAOJdbc extends AbstractDAOJdbc<City> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingDAOJdbc.class);

    public BookingDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public boolean insertBookingCar(int userId, int carId, String dateFrom, String dateTo, long totalPrice) throws DAOException {
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("INSERT_BOOKING_INFORM");
        try{
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setInt(1, userId);
            pst.setInt(2, carId);
            pst.setString(3, dateFrom);
            pst.setString(4, dateTo);
            pst.setLong(5, totalPrice);
            pst.executeUpdate();

            LOGGER.info("Update booking car commit");
            return true;
        } catch (SQLException e) {
            LOGGER.error("Cannot update inform about booking", e);
            return false;
        }
    }

    public List<Booking> getAllBookingByID() throws DAOException {
        List<Booking> bookings = new ArrayList<>();
        try{
            String query = getQuery("BOOKING_GET_ALL");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                Booking booking = new Booking();
                booking.setId(resultSet.getInt("id"));
                booking.setUser(getUser(resultSet.getInt("user_id")));
                booking.setCar(getCar(resultSet.getInt("car_id")));
                booking.setFrom(resultSet.getTimestamp("dateFrom"));
                booking.setTo(resultSet.getTimestamp("dateTo"));
                booking.setTotalPrice(resultSet.getInt("price"));
                bookings.add(booking);
            }
        } catch (SQLException e) {
            LOGGER.error("Dao get all bookings is failed", e);
            throw new DAOException("Dao get all bookings is failed");
        }
        return bookings;
    }

    private Car getCar(int car_id) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.getCarsById(car_id);
        } catch (DAOException e) {
            LOGGER.error("Cannot get car by id", e);
            throw new ServiceException("Cannot get car by id");
        }
    }

    private User getUser(int user_id) {
        try{
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            return daoJdbc.findById(user_id, User.class);
        } catch (DAOException e) {
            LOGGER.error("Cannot get user by id", e);
            throw new ServiceException("Cannot get user by id");
        }
    }

    public void delete(Booking booking) {
        LOGGER.info("Booking for delete: " + booking.toString());
        try{
            ResourceBundle resource = ResourceBundle.getBundle("crud");
            String query = resource.getString("DELETE_BOOKING");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, booking.getId());
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Dao delete booking operation is failed");
            e.printStackTrace();
        }
    }

    public Booking getBookingById(int id) throws DAOException {
        try{
            String query = getQuery("BOOKING_GET_BY_ID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            Booking booking = null;
            if(resultSet.next()){
                booking = new Booking();
                booking.setId(resultSet.getInt("id"));
                booking.setUser(getUser(resultSet.getInt("user_id")));
                booking.setCar(getCar(resultSet.getInt("car_id")));
                booking.setFrom(resultSet.getTimestamp("dateFrom"));
                booking.setTo(resultSet.getTimestamp("dateTo"));
                booking.setTotalPrice(resultSet.getInt("price"));
            }
            return booking;
        } catch (SQLException e) {
            LOGGER.error("Dao get booking by id is failed", e);
            throw new DAOException("Dao get booking by id is failed");
        }
    }
}
