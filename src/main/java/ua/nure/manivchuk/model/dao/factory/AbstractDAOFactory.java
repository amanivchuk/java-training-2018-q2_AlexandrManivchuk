package ua.nure.manivchuk.model.dao.factory;

import ua.nure.manivchuk.model.dao.AbstractDAO;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;

public abstract class AbstractDAOFactory {
    public abstract <T extends AbstractDAO> T getDAO(Class<T> clazz) throws DAOException;
}
