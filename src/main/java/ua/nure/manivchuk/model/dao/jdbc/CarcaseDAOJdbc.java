package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Carcase;
import ua.nure.manivchuk.model.entity.Classific;
import ua.nure.manivchuk.model.entity.Fuel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CarcaseDAOJdbc extends AbstractDAOJdbc<Classific> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CarcaseDAOJdbc.class);

    public CarcaseDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public ArrayList<Carcase> getAllCarcases() throws DAOException {
        ArrayList<Carcase> carcases = new ArrayList<>();
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("SELECT_CARCASE_LIST");
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Carcase carcase = new Carcase();
                carcase.setId(resultSet.getInt("carcase_id"));
                carcase.setCarcase(resultSet.getString("carcase"));
                carcases.add(carcase);
            }
            return carcases;
        } catch (SQLException e) {
            LOGGER.error("Cannot get all carcases", e);
            throw new DAOException("Cannot get all carcases");
        }
    }

    public Carcase getCarcaseByName(String carcaseName) throws DAOException {
        Carcase carcase = null;
        try {
            String query = getQuery("SELECT_CARCASE_BY_NAME");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, carcaseName);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                carcase = new Carcase();
                carcase.setId(resultSet.getInt("carcase_id"));
            }
            return carcase;
        } catch (SQLException e) {
            LOGGER.error("Cannot get carcase by name ", e);
            throw new DAOException("Cannot get carcase by name");
        }
    }
}
