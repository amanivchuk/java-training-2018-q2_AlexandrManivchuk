package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Classific;
import ua.nure.manivchuk.model.entity.Fuel;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class FotoDAOJdbc extends AbstractDAOJdbc<Classific> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FotoDAOJdbc.class);

    public FotoDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public void update(InputStream file, int car_id) throws DAOException {
        try{
            String query = getQuery("CREATE_FOTO");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setBlob(1, file);
            statement.setInt(2, car_id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("Dao create foto is failed", e);
            throw new DAOException("Dao create foto is failed");
        }
    }

}
