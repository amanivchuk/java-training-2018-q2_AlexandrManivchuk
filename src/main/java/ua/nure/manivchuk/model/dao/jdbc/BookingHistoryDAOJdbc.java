package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.AbstractDAOFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.entity.*;
import ua.nure.manivchuk.service.ServiceException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class BookingHistoryDAOJdbc extends AbstractDAOJdbc<City> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingHistoryDAOJdbc.class);

    public BookingHistoryDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public boolean insertBookingHistoryCar(int userId, int carId, String dateFrom, String dateTo, long totalPrice, String status) throws DAOException {
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("INSERT_BOOKING_HISTORY_INFORM");
        try{
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setInt(1, userId);
            pst.setInt(2, carId);
            pst.setString(3, dateFrom);
            pst.setString(4, dateTo);
            pst.setLong(5, totalPrice);
            pst.setString(6, status);
            pst.executeUpdate();

            LOGGER.info("Update booking history car commit");
            return true;
        } catch (SQLException e) {
            LOGGER.error("Cannot update booking inform about booking", e);
            return false;
        }
    }

    public List<BookingHistory> getAllBookingHistoryByUserID(int user_id) throws DAOException {
        List<BookingHistory> bookings = new ArrayList<>();
        try{
            String query = getQuery("BOOKING_HISTORY_GET_ALL_BY_USER_ID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, user_id);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                BookingHistory booking = new BookingHistory();
                booking.setId(resultSet.getInt("id"));
                booking.setUser(getUser(resultSet.getInt("user_id")));
                booking.setCar(getCar(resultSet.getInt("car_id")));
                booking.setFrom(resultSet.getTimestamp("dateFrom"));
                booking.setTo(resultSet.getTimestamp("dateTo"));
                booking.setTotalPrice(resultSet.getInt("price"));
                booking.setStatus(resultSet.getString("status_order"));
                bookings.add(booking);
            }
        } catch (SQLException e) {
            LOGGER.error("Dao get all bookings history is failed", e);
            throw new DAOException("Dao get all bookings history is failed");
        }
        return bookings;
    }

    private Car getCar(int car_id) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.getCarsById(car_id);
        } catch (DAOException e) {
            LOGGER.error("Cannot get car by id", e);
            throw new ServiceException("Cannot get car by id");
        }
    }

    private User getUser(int user_id) {
        try{
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            return daoJdbc.findById(user_id, User.class);
        } catch (DAOException e) {
            LOGGER.error("Cannot get user by id", e);
            throw new ServiceException("Cannot get user by id");
        }
    }

    public void delete(BookingHistory booking) {
        LOGGER.info("Booking for delete: " + booking.toString());
        try{
            ResourceBundle resource = ResourceBundle.getBundle("crud");
            String query = resource.getString("DELETE_BOOKING_HISTORY");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, booking.getId());
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Dao delete booking history operation is failed");
            e.printStackTrace();
        }
    }

    public BookingHistory getBookingHistoryById(int user_id) throws DAOException {
        try{
            String query = getQuery("BOOKING_HISTORY_GET_BY_USER_ID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, user_id);
            ResultSet resultSet = statement.executeQuery();
            BookingHistory booking = null;
            if(resultSet.next()){
                booking = new BookingHistory();
                booking.setId(resultSet.getInt("id"));
                booking.setUser(getUser(resultSet.getInt("user_id")));
                booking.setCar(getCar(resultSet.getInt("car_id")));
                booking.setFrom(resultSet.getTimestamp("dateFrom"));
                booking.setTo(resultSet.getTimestamp("dateTo"));
                booking.setTotalPrice(resultSet.getInt("price"));
                booking.setStatus(resultSet.getString("status_order"));
            }
            return booking;
        } catch (SQLException e) {
            LOGGER.error("Dao get booking history by user_id is failed", e);
            throw new DAOException("Dao get booking history by user_id is failed");
        }
    }
}
