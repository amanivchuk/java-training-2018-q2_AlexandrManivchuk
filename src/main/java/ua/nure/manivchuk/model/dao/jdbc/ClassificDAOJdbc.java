package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Classific;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ClassificDAOJdbc extends AbstractDAOJdbc<Classific> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassificDAOJdbc.class);

    public ClassificDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public ArrayList<Classific> getAllClassifics() throws DAOException {
        ArrayList<Classific> classifics = new ArrayList<>();
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("SELECT_CLASSIFIC_LIST");
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Classific classific = new Classific();
                classific.setId(resultSet.getInt("classific_id"));
                classific.setClassific(resultSet.getString("classific"));
                classifics.add(classific);
            }
            return classifics;
        } catch (SQLException e) {
            LOGGER.error("Cannot get all classifics", e);
            throw new DAOException("Cannot get all classifics");
        }
    }

    public Classific getClassificId(String classificName) throws DAOException {

        LOGGER.info("CLASSIFIC NAME = " + classificName);
        Classific classific = null;
        try {
            String query = getQuery("SELECT_CLASSIFIC_ID_BY_NAME");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, classificName);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                classific = new Classific();
                classific.setId(resultSet.getInt("classific_id"));
            }
            LOGGER.info("CLASSIFIC = " + classific);
            return classific;
        } catch (SQLException e) {
            LOGGER.error("Cannot get classific id by name ", e);
            throw new DAOException("Cannot get classific id by name");
        }
    }
}
