package ua.nure.manivchuk.model.dao.jdbc;

import ua.nure.manivchuk.model.dao.AbstractDAO;
import ua.nure.manivchuk.model.dao.factory.EntityFactory;
import ua.nure.manivchuk.model.entity.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public abstract class AbstractDAOJdbc<T extends BaseEntity> extends AbstractDAO {
    private final static Logger LOGGER = LoggerFactory.getLogger(AbstractDAOJdbc.class);
    Connection connection;

    public T findById(int id, Class<T> tClass) throws DAOException{
        T object;
        try{
            ResourceBundle resource = ResourceBundle.getBundle("crud");
            String key = tClass.getSimpleName().toUpperCase() + "_GET_BY_ID";
            String query = resource.getString(key);
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            EntityFactory<T> factory = new EntityFactory<>();
            object = factory.getInstanceFromResSet(set, tClass);
        } catch (SQLException e) {
            LOGGER.error("Cannot find by ID");
            e.printStackTrace();
            throw new DAOException("Cannot find by ID");
        }
        return object;
    }

    public int generatedKeyGetter(PreparedStatement statement) throws DAOException{
        try{
            ResultSet keys = statement.getGeneratedKeys();
            int idToReturn = 0;
            if(keys.next()){
                idToReturn = keys.getInt(1);
            }
            if(idToReturn == 0){
                throw new SQLException("Cannot obtain generated id");
            }
            return idToReturn;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DAOException("Cannot obtain generated keys");
        }
    }

    public void closeStatement(PreparedStatement statement){
        try{
            statement.close();
        } catch (SQLException e) {
            LOGGER.error("Cannot close statement", e);
            e.printStackTrace();
        }catch (NullPointerException e){
            LOGGER.error("Connection is null");
        }
    }

    public void closeResultSet(ResultSet resultSet){
        try{
            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error("Cannot close result set", e);
            e.printStackTrace();
        }catch (NullPointerException e){
            LOGGER.error("Connection is null");
        }
    }

    public String getQuery(String key){
        ResourceBundle bundle = ResourceBundle.getBundle("crud");
        return bundle.getString(key);
    }
}
