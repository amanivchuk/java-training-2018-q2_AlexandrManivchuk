package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.EntityFactory;
import ua.nure.manivchuk.model.entity.*;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CarsDAOJdbc extends AbstractDAOJdbc<Car> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CarsDAOJdbc.class);

    public CarsDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public List<Car> getCarsByCity(int city) throws DAOException {
        ArrayList<Car> listCar = new ArrayList<>();
        LOGGER.info("getCarsByCity ------->>> " + city);
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_CARS_BY_CITY");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, city);
            ResultSet resultSet = statement.executeQuery();
            Car car = null;
            while(resultSet.next()){
                car = getCar(resultSet);
                System.out.println("SELECTED CAR===========> " + car);
                listCar.add(car);
            }

            return listCar;
        } catch (SQLException | DAOException e) {
            LOGGER.error("Dao find car by city is failed", e);
            throw new DAOException("Dao find car by city is failed");
        }
    }

    public List<Car> getAllUsersCarsSort(int city, String sortBy) throws DAOException {
        ArrayList<Car> listCar = new ArrayList<>();
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_CARS_BY_SORT");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, city);
            statement.setString(2, sortBy);
            ResultSet resultSet = statement.executeQuery();
            Car car = null;
            while(resultSet.next()){
                car = getCar(resultSet);
                System.out.println("SELECTED CAR===========> " + car);
                listCar.add(car);
            }

            return listCar;
        } catch (SQLException | DAOException e) {
            LOGGER.error("Dao find car by city is failed", e);
            throw new DAOException("Dao find car by city is failed");
        }
    }

    public boolean updateCarStatus(int carId){
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("UPDATE_CAR_STATUS_BOOKING");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, 0);
            statement.setInt(2, carId);
            statement.executeUpdate();

            LOGGER.info("Car status was changed on booking status");

            return true;
        } catch (SQLException e) {
            LOGGER.error("Cannot update status car after booking", e);
            return false;
        }
    }

    public boolean updateCarStatusAfterRemoveBooking(int carId){
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("UPDATE_CAR_STATUS_BOOKING");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, 1);
            statement.setInt(2, carId);
            statement.executeUpdate();

            LOGGER.info("Car status was changed on booking status");

            return true;
        } catch (SQLException e) {
            LOGGER.error("Cannot update status car after booking", e);
            return false;
        }
    }

    public Classific selectClassific(int id_classific) throws DAOException {
        Classific classific;
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_CLASSIFIC_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id_classific);
            ResultSet set = statement.executeQuery();
            EntityFactory<Classific> factory = new EntityFactory<>();
            classific = factory.getInstanceFromResSet(set, Classific.class);
        } catch (SQLException | DAOException e) {
            throw new DAOException("Dao find by id_classific is failed");
        }
        return classific;
    }

    private Car getCar(ResultSet resultSet) throws SQLException, DAOException {
        Car car = new Car();
        car.setId(resultSet.getInt("car_id"));
        car.setAvailability(resultSet.getBoolean("availability"));
        car.setMark(resultSet.getString("mark"));
        car.setIssuecontry(resultSet.getString("issuecontry"));
        car.setYear(resultSet.getInt("year"));
        car.setPrice(resultSet.getInt("price"));
        car.setClassific(selectClassific(resultSet.getInt("classifics_id")));
        car.setCarcase(selectCarcase(resultSet.getInt("carscases_id")));
        car.setFuel(selectFuel(resultSet.getInt("fuels_id")));
//        car.setRulesrent(selectRulesrent(resultSet.getInt("rulesrent_id")));
        car.setRulesrent(selectRulesrent2(car.getId()));
        car.setTransmission(selectTransmission(resultSet.getInt("transmissions_id")));
        car.setAvailability(resultSet.getBoolean("availability"));
        car.setCity(selectCity(resultSet.getInt("city_id")));
        car.setImage(resultSet.getBytes("images"));
        car.setPhotos(selectPhotosById(car.getId()));

        return car;
    }

    private ArrayList<Rulesrent> selectRulesrent2(int car_id) {
        ArrayList<Rulesrent> list = new ArrayList<>();
        try {
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_RULERENTS_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, car_id);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()){
                Rulesrent rulesrent = new Rulesrent();
                rulesrent.setId(resultSet.getInt("rulerent_id"));
                rulesrent.setRulerent(resultSet.getString("rulerent"));
                LOGGER.info("RULERENT from DB =====> " + rulesrent);
                list.add(rulesrent);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private ArrayList<Photos> selectPhotosById(int id) throws DAOException {
        ArrayList<Photos> listPhotos = new ArrayList<>();
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_PHOTOS_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            Photos photo;
            while(resultSet.next()){
                photo = getPhoto(resultSet);
                listPhotos.add(photo);
            }
            return listPhotos;
        } catch (SQLException e) {
            LOGGER.info("Cannot get list of fotos from Databace", e);
            throw new DAOException("Cannot get list of fotos from Databace");
        }
    }

    private Photos getPhoto(ResultSet resultSet) throws SQLException {
        Photos photo = new Photos();
        photo.setId(resultSet.getInt("id"));
        photo.setImage(resultSet.getBytes("foto"));
        photo.setCars_id(resultSet.getInt("cars_id"));
        return photo;
    }

    private City selectCity(int city_id) throws DAOException {
        City city;
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_CITY_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, city_id);
            ResultSet set = statement.executeQuery();
            EntityFactory<City> factory = new EntityFactory<>();
            city = factory.getInstanceFromResSet(set, City.class);
        } catch (SQLException | DAOException e) {
            throw new DAOException("Dao find by id_city is failed");
        }
        return city;
    }

    private Transmission selectTransmission(int transmissions_id) throws DAOException {
        Transmission transmission;
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_TRANSMISSION_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, transmissions_id);
            ResultSet set = statement.executeQuery();
            EntityFactory<Transmission> factory = new EntityFactory<>();
            transmission = factory.getInstanceFromResSet(set, Transmission.class);
        } catch (SQLException | DAOException e) {
            throw new DAOException("Dao find by id_transmission is failed");
        }
        return transmission;
    }

    private Rulesrent selectRulesrent(int rulesrent_id) throws DAOException {
        Rulesrent rulesrent;
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_RULESRENT_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, rulesrent_id);
            ResultSet set = statement.executeQuery();
            EntityFactory<Rulesrent> factory = new EntityFactory<>();
            rulesrent = factory.getInstanceFromResSet(set, Rulesrent.class);
        } catch (SQLException | DAOException e) {
            throw new DAOException("Dao find by id_rulesrent is failed");
        }
        return rulesrent;
    }

    private Fuel selectFuel(int fuels_id) throws DAOException {
        Fuel fuel;
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_FUEL_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, fuels_id);
            ResultSet set = statement.executeQuery();
            EntityFactory<Fuel> factory = new EntityFactory<>();
            fuel = factory.getInstanceFromResSet(set, Fuel.class);
        } catch (SQLException | DAOException e) {
            throw new DAOException("Dao find by id_fuel is failed");
        }
        return fuel;
    }

    private Carcase selectCarcase(int carscases_id) throws DAOException {
        Carcase carcase;
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_CARCASE_BY_ID_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, carscases_id);
            ResultSet set = statement.executeQuery();
            EntityFactory<Carcase> factory = new EntityFactory<>();
            carcase = factory.getInstanceFromResSet(set, Carcase.class);
        } catch (SQLException | DAOException e) {
            throw new DAOException("Dao find by id_classific is failed");
        }
        return carcase;
    }

    public List<Car> getAllCars() throws DAOException {
        List<Car> cars = new ArrayList<>();
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("CAR_GET_ALL");
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            Car car = null;
            while(resultSet.next()){
                car = getCar(resultSet);
                System.out.println("===> " + car);
                cars.add(car);
            }

            return cars;
        } catch (SQLException | DAOException e) {
            LOGGER.error("Cannot Get All Cars", e);
            e.printStackTrace();
            throw new DAOException("Cannot Get All Cars");
        }
    }

    public void delete(Car car) {
        LOGGER.info("Car for delete: " + car.toString());
        try{
            ResourceBundle resource = ResourceBundle.getBundle("crud");
            String query = resource.getString("DELETE_CAR");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, car.getId());
            statement.execute();
        } catch (SQLException e) {
            LOGGER.error("Dao delete car operation is failed");
            e.printStackTrace();
        }
    }

    public Car getCarsById(int id) throws DAOException {
        ArrayList<Car> listCar = new ArrayList<>();
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_CARS_BY_ID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            Car car = null;
            if(resultSet.next()){
                car = getCar(resultSet);
                System.out.println("===> " + car);
            }
            return car;
        } catch (SQLException | DAOException e) {
            LOGGER.error("Dao find car by city is failed", e);
            throw new DAOException("Dao find car by city is failed");
        }
    }

    public int create(Car car, InputStream file1) throws DAOException {
        try{
            String query = getQuery("CREATE_CAR");
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, car.getMark());
            statement.setLong(2, car.getPrice());
            statement.setInt(3, car.getClassific().getId());
            statement.setInt(4, car.getCarcase().getId());
            statement.setInt(5, car.getFuel().getId());
            statement.setInt(6, car.getTransmission().getId());
            statement.setInt(7, car.getCity().getId());
            statement.setInt(8, 1);
            statement.setBlob(9, file1);
            statement.executeUpdate();
            return generatedKeyGetter(statement);
        } catch (SQLException | DAOException e) {
            LOGGER.error("Dao create car is failed", e);
            throw new DAOException("Dao create car is failed");
        }
    }

    public List<Car> getAllUsersCarsById(int id) throws DAOException {
        ArrayList<Car> listCar = new ArrayList<>();
        try{
            ResourceBundle resourceBundle = ResourceBundle.getBundle("crud");
            String query = resourceBundle.getString("SELECT_USERS_CARS_BY_USER_ID");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            Car car = null;
            while(resultSet.next()){
                car = getCar(resultSet);
                listCar.add(car);
            }
            return listCar;
        } catch (SQLException | DAOException e) {
            LOGGER.error("Dao find car by id is failed", e);
            throw new DAOException("Dao find car by id is failed");
        }
    }
}
