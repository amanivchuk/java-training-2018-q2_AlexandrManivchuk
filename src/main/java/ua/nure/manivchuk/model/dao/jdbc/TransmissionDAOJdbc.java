package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Classific;
import ua.nure.manivchuk.model.entity.Transmission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class TransmissionDAOJdbc extends AbstractDAOJdbc<Classific> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransmissionDAOJdbc.class);

    public TransmissionDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public ArrayList<Transmission> getAllTransmissions() throws DAOException {
        ArrayList<Transmission> transmissions = new ArrayList<>();
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("SELECT_TRANSMISSION_LIST");
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                Transmission transmission = new Transmission();
                transmission.setId(resultSet.getInt("id_transmission"));
                transmission.setTransmission(resultSet.getString("transmission"));
                transmissions.add(transmission);
            }
            return transmissions;
        } catch (SQLException e) {
            LOGGER.error("Cannot get all transmissions", e);
            throw new DAOException("Cannot get all transmissions");
        }
    }

    public Transmission getTransmissionId(String transmissionName) throws DAOException {
        Transmission transmission = null;
        try {
            String query = getQuery("SELECT_TRANSMISSION_ID_BY_NAME");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, transmissionName);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                transmission = new Transmission();
                transmission.setId(resultSet.getInt("id_transmission"));
            }
            return transmission;
        } catch (SQLException e) {
            LOGGER.error("Cannot get transmission id by name ", e);
            throw new DAOException("Cannot get transmission id by name");
        }
    }
}
