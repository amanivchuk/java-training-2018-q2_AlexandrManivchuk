package ua.nure.manivchuk.model.dao.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.City;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class CityDAOJdbc extends AbstractDAOJdbc<City> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CityDAOJdbc.class);

    public CityDAOJdbc(Connection connection) {
        this.connection = connection;
    }

    public List<City> getAllCity() throws DAOException {
        ArrayList<City> cities = new ArrayList<>();
        ResourceBundle resource = ResourceBundle.getBundle("crud");
        String query = resource.getString("SELECT_CITY_LIST");
        try{
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                City city = new City();
                city.setCity(resultSet.getString("city"));
                city.setCountry(resultSet.getString("country"));
                city.setData(resultSet.getString("data"));
                cities.add(city);
            }
            return cities;
        } catch (SQLException e) {
            LOGGER.error("Cannot get all cities", e);
            throw new DAOException("Cannot get all cities");
        }
    }

    public City getCityId(String cityName) throws DAOException {
        City city = null;
        try {
            String query = getQuery("SELECT_CITY_ID_BY_NAME");
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, cityName);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                city = new City();
                city.setId(resultSet.getInt("id"));
            }
            return city;
        } catch (SQLException e) {
            LOGGER.error("Cannot get city id by name ", e);
            throw new DAOException("Cannot get city id by name");
        }
    }
}
