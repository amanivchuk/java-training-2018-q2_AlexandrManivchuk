package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.CarcaseDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.dao.jdbc.FuelDAOJdbc;
import ua.nure.manivchuk.model.entity.Carcase;
import ua.nure.manivchuk.model.entity.Fuel;

import java.util.ArrayList;

public class CarcaseService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CarcaseService.class);

    public ArrayList<Carcase> getAllCarcases(){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CarcaseDAOJdbc daoJdbc = factory.getDAO(CarcaseDAOJdbc.class);
            return daoJdbc.getAllCarcases();
        } catch (DAOException e) {
            LOGGER.error("Cannot get all carcase", e);
            throw new ServiceException("Cannot get all carcase");
        }
    }
}
