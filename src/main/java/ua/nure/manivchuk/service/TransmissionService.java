package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.ClassificDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.dao.jdbc.TransmissionDAOJdbc;
import ua.nure.manivchuk.model.entity.Classific;
import ua.nure.manivchuk.model.entity.Transmission;

import java.util.ArrayList;

public class TransmissionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransmissionService.class);

    public ArrayList<Transmission> getAllTransmissons(){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            TransmissionDAOJdbc daoJdbc = factory.getDAO(TransmissionDAOJdbc.class);
            return daoJdbc.getAllTransmissions();
        } catch (DAOException e) {
            LOGGER.error("Cannot get all transmissions", e);
            throw new ServiceException("Cannot get all transmissions");
        }
    }
}
