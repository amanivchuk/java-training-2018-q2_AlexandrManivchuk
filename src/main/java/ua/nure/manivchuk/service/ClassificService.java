package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.ClassificDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.entity.Classific;

import java.util.ArrayList;

public class ClassificService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassificService.class);

    public ArrayList<Classific> getAllClassifics(){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            ClassificDAOJdbc daoJdbc = factory.getDAO(ClassificDAOJdbc.class);
            return daoJdbc.getAllClassifics();
        } catch (DAOException e) {
            LOGGER.error("Cannot get all classific", e);
            throw new ServiceException("Cannot get all classific");
        }
    }
}
