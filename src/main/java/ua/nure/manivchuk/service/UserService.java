package ua.nure.manivchuk.service;

import ua.nure.manivchuk.model.dao.factory.AbstractDAOFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.dao.jdbc.UserDAOJdbc;
import ua.nure.manivchuk.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.List;

public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    public long registerUser(User user){
        try{
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            UserDAOJdbc dao = factory.getDAO(UserDAOJdbc.class);
            long key = dao.create(user);
            return key;
        } catch (DAOException e) {
            LOGGER.error("Cannot register new user");
            e.printStackTrace();
            throw new ServiceException("Cannot register new user");
        }
    }

    public boolean isEmailExist(String email){
        try{
            boolean result = false;
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            User user = daoJdbc.findByEmail(email);
            if(user.getEmail() != null){
                result = true;
            }
            return result;
        } catch (DAOException e) {
            LOGGER.error("Cannot check if email exist");
            e.printStackTrace();
            throw new ServiceException("Cannot chaeck if email exist");
        }
    }

    public boolean isValidPair(String email, String password){
        try{
            boolean isValid = false;
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            User user = daoJdbc.findByEmail(email);
            if(user.getPassword().equals(password)){
                isValid = true;
            }
            /*PasswordHandler handler = new PasswordHandler();
            String hashedPass = handler.getHashedPassword(password);
            String passFromBase = user.getPassword();
            if(hashedPass.equals(passFromBase)){
                isValid = true;
            }*/
            return isValid;
        } catch (DAOException e) {
            LOGGER.error("Cannot check is pair valid" ,e);
            throw new ServiceException("Cannot check is pair valid");
        }
    }

    public User getUserByEmail(String email){
        try{
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            return daoJdbc.findByEmail(email);
        } catch (DAOException e) {
            LOGGER.error("Cannot get user by id", e);
            throw new ServiceException("Cannot get user by id");
        }
    }

    public User getUserById(int id){
        try{
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            return daoJdbc.findById(id, User.class);
        } catch (DAOException e) {
            LOGGER.error("Cannot get user by id", e);
            throw new ServiceException("Cannot get user by id");
        }
    }

    public List<User> getAllUser(){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            return daoJdbc.getAllUser();
        } catch (DAOException e) {
            LOGGER.error("Cannot get All Users", e);
            throw new ServiceException("Cannot get All Users");
        }
    }

    public void deleteUser(int id) {
        DaoFactoryJdbc factory = null;
        try{
            factory = new DaoFactoryJdbc();
            factory.startTransaction();
            UserDAOJdbc userDAO = factory.getDAO(UserDAOJdbc.class);
            User user = new User();
            user.setId(id);
            userDAO.delete(user);
            factory.commitTransaction();
        } catch (DAOException e) {
            try{
                factory.roolbackTransaction();
            } catch (DAOException e1) {
                LOGGER.error("Cannot rollback transaction");
                new ServiceException("Cannot rollback transaction");
            }
            LOGGER.error("Cannot delete user");
            e.printStackTrace();
            throw new ServiceException("Cannot delete user");
        }finally {
            factory.closeConnection();
        }
    }

    public User updateUser(User user) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            daoJdbc.updateUser(user);
            return daoJdbc.getUserById(user.getId());
        } catch (DAOException e) {
            LOGGER.error("Cannot update user", e);
            throw new ServiceException("Cannot update user");
        }
    }

    public User updateUserWithPhoto(User user, InputStream file) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            daoJdbc.updateUserWithPhoto(user, file);
            return daoJdbc.getUserById(user.getId());
        } catch (DAOException e) {
            LOGGER.error("Cannot update user with photo", e);
            throw new ServiceException("Cannot update user with photo");
        }
    }

    public User updatePasswordUser(String newpassword, User currentUser) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            daoJdbc.updatePasswordUser(newpassword, currentUser.getId());
            return daoJdbc.getUserById(currentUser.getId());
        } catch (DAOException e) {
            LOGGER.error("Cannot update user password", e);
            throw new ServiceException("Cannot update user password");
        }
    }

    public void updateFotoDocument(InputStream file, int user_id, String attributeName) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            UserDAOJdbc daoJdbc = factory.getDAO(UserDAOJdbc.class);
            daoJdbc.updateUserFotoDocument(file, user_id, attributeName);
        } catch (DAOException e) {
            LOGGER.error("Cannot update user photo document", e);
            throw new ServiceException("Cannot update user photo document");
        }
    }

    public void blockUser(int id, int statusBlock) {
        DaoFactoryJdbc factory = null;
        try{
            factory = new DaoFactoryJdbc();
            factory.startTransaction();
            UserDAOJdbc userDAO = factory.getDAO(UserDAOJdbc.class);
            User user = new User();
            user.setId(id);
            userDAO.block(user, statusBlock);
            factory.commitTransaction();
        } catch (DAOException e) {
            try{
                factory.roolbackTransaction();
            } catch (DAOException e1) {
                LOGGER.error("Cannot rollback transaction");
                new ServiceException("Cannot rollback transaction");
            }
            LOGGER.error("Cannot delete user");
            e.printStackTrace();
            throw new ServiceException("Cannot delete user");
        }finally {
            factory.closeConnection();
        }
    }
}
