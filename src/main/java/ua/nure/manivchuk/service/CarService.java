package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.AbstractDAOFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.*;
import ua.nure.manivchuk.model.entity.*;

import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class CarService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CarService.class);

    public List<Car> getAllCarsByCity(String city){
        try{
            LOGGER.info("getAllCarsByCity ========>>>> " + city);
            DaoFactoryJdbc factory = new DaoFactoryJdbc();

            CityDAOJdbc cityDAOJdbc = factory.getDAO(CityDAOJdbc.class);
            City cityDB = cityDAOJdbc.getCityId(city);

            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.getCarsByCity(cityDB.getId());
        } catch (DAOException e) {
            LOGGER.error("Cannot get cars by city", e);
            throw new ServiceException("Cannot get cars by city");
        }
    }

    public boolean updateCarStatus(int carId){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.updateCarStatus(carId);
        } catch (DAOException e) {
            LOGGER.error("Cannot change car status", e);
            throw new ServiceException("Cannot change car status");
        }
    }

    public List<Car> getAllCar() {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.getAllCars();
        } catch (DAOException e) {
            LOGGER.error("Cannot get All Cars", e);
            throw new ServiceException("Cannot get All Cars");
        }
    }

    public Car getCarById(int id) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.getCarsById(id);
        } catch (DAOException e) {
            LOGGER.error("Cannot get car by id", e);
            throw new ServiceException("Cannot get car by id");
        }
    }

    public void deleteCar(int id){
        DaoFactoryJdbc factory = null;
        try{
            factory = new DaoFactoryJdbc();
            factory.startTransaction();
            CarsDAOJdbc carDAO = factory.getDAO(CarsDAOJdbc.class);
            Car car = new Car();
            car.setId(id);
            carDAO.delete(car);
            factory.commitTransaction();
        } catch (DAOException e) {
            try{
                factory.roolbackTransaction();
            } catch (DAOException e1) {
                LOGGER.error("Cannot rollback transaction");
                new ServiceException("Cannot rollback transaction");
            }
            LOGGER.error("Cannot delete user");
            e.printStackTrace();
            throw new ServiceException("Cannot delete car");
        }finally {
            factory.closeConnection();
        }
    }

    public void createCar(String carName, String city, String[] rulesrentArray, String transmission, String classific, String fuel, String price, User user, String carcase, InputStream file1, InputStream file2, InputStream file3) {
        DaoFactoryJdbc factory = null;
        try{
            factory = new DaoFactoryJdbc();
            factory.startTransaction();

            CityDAOJdbc cityDAO = factory.getDAO(CityDAOJdbc.class);
            TransmissionDAOJdbc transmissionDAO = factory.getDAO(TransmissionDAOJdbc.class);
            FuelDAOJdbc fuelDAO = factory.getDAO(FuelDAOJdbc.class);
            ClassificDAOJdbc classificDAO = factory.getDAO(ClassificDAOJdbc.class);
            RulesrentDAOJdbc rulesrentDAO = factory.getDAO(RulesrentDAOJdbc.class);
            UserDAOJdbc userDAO = factory.getDAO(UserDAOJdbc.class);
            CarcaseDAOJdbc carcaseDAO = factory.getDAO(CarcaseDAOJdbc.class);

            City cityDB = cityDAO.getCityId(city);
            Classific classificDB = classificDAO.getClassificId(classific);
            Transmission transmissionDB = transmissionDAO.getTransmissionId(transmission);
            Fuel fuelDB = fuelDAO.getFuelId(fuel);
            Carcase carcas = carcaseDAO.getCarcaseByName(carcase);
            ArrayList<Rulesrent> rulesrentDB = rulesrentDAO.getRulesrentByName(rulesrentArray);

            CarsDAOJdbc addCar = factory.getDAO(CarsDAOJdbc.class);
            Car newCar = new Car();
            newCar.setMark(carName);
            newCar.setPrice(Integer.parseInt(price));
            newCar.setCity(cityDB);
            newCar.setClassific(classificDB);
            newCar.setTransmission(transmissionDB);
            newCar.setFuel(fuelDB);
            newCar.setRulesrent(rulesrentDB);
            newCar.setCarcase(carcas);

            int car_id = addCar.create(newCar, file3);
            LOGGER.info("=========> new car_id ====:" + car_id);

            FotoDAOJdbc foto = factory.getDAO(FotoDAOJdbc.class);
            foto.update(file1, car_id);
            foto.update(file2, car_id);
            LOGGER.info("Fotos added to DB");

            for(Rulesrent r : newCar.getRulesrent()) {
                rulesrentDAO.updateRulerentByCarId(car_id, r.getId());
            }
            userDAO.updateCarUser(user.getId(), car_id);
            LOGGER.info("Car was added to DB");
            factory.commitTransaction();
        } catch (DAOException e) {
            try {
                factory.roolbackTransaction();
                LOGGER.info("transaction rollback done");
            } catch (DAOException e1) {
                throw new ServiceException("Cannot create");
            }
            LOGGER.error("Cannot create new car");
            e.printStackTrace();;
            throw new ServiceException("Cannot create new car");
        } finally {
            factory.closeConnection();
        }
    }

    public List<Car> getAllUsersCarById(int id) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.getAllUsersCarsById(id);
        } catch (DAOException e) {
            LOGGER.error("Cannot get car by id", e);
            throw new ServiceException("Cannot get car by id");
        }
    }

    public List<Car> getAllCarsSort(String city, String sortBy) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CityDAOJdbc cityDAOJdbc = factory.getDAO(CityDAOJdbc.class);
            City cityDB = cityDAOJdbc.getCityId(city);

            CarsDAOJdbc daoJdbc = factory.getDAO(CarsDAOJdbc.class);
            return daoJdbc.getAllUsersCarsSort(cityDB.getId(), sortBy);
        } catch (DAOException e) {
            LOGGER.error("Cannot get car sorted", e);
            throw new ServiceException("Cannot get car sorted");
        }
    }
}
