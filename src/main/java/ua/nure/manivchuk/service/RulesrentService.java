package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.dao.jdbc.RulesrentDAOJdbc;
import ua.nure.manivchuk.model.entity.Rulesrent;

import java.util.ArrayList;

public class RulesrentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RulesrentService.class);

    public ArrayList<Rulesrent> getAllClassifics(){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            RulesrentDAOJdbc daoJdbc = factory.getDAO(RulesrentDAOJdbc.class);
            return daoJdbc.getAllRulesrent();
        } catch (DAOException e) {
            LOGGER.error("Cannot get all rulesrents", e);
            throw new ServiceException("Cannot get all rulesrents");
        }
    }
}
