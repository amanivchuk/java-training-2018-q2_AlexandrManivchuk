package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.AbstractDAOFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.BookingDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.CarsDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.CityDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.entity.Booking;
import ua.nure.manivchuk.model.entity.City;

import java.util.List;

public class BookingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingService.class);

    public boolean createBooking(int userId, int carId, String dateFrom, String dateTo, long totalPrice){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            BookingDAOJdbc daoJdbc = factory.getDAO(BookingDAOJdbc.class);
            return daoJdbc.insertBookingCar(userId, carId, dateFrom, dateTo, totalPrice);
        } catch (DAOException e) {
            LOGGER.error("Cannot booking car", e);
            throw new ServiceException("Cannot booking car");
        }
    }

    public List<Booking> getAllBookings(){
        try{
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            BookingDAOJdbc bookingDAO = factory.getDAO(BookingDAOJdbc.class);
            return bookingDAO.getAllBookingByID();
        } catch (DAOException e) {
            throw new ServiceException("Cannot get all booking");
        }
    }

    public void deleteBooking(int id, Booking booking) {
        DaoFactoryJdbc factory = null;
        try{
            factory = new DaoFactoryJdbc();
            factory.startTransaction();
            BookingDAOJdbc bookingDAO = factory.getDAO(BookingDAOJdbc.class);
            booking.setId(id);
            bookingDAO.delete(booking);
            CarsDAOJdbc carDAO = factory.getDAO(CarsDAOJdbc.class);
            carDAO.updateCarStatusAfterRemoveBooking(booking.getCar().getId());

            factory.commitTransaction();
        } catch (DAOException e) {
            try{
                factory.roolbackTransaction();
            } catch (DAOException e1) {
                LOGGER.error("Cannot rollback transaction");
                new ServiceException("Cannot rollback transaction");
            }
            LOGGER.error("Cannot delete user");
            e.printStackTrace();
            throw new ServiceException("Cannot delete car");
        }finally {
            factory.closeConnection();
        }
    }

    public Booking getBookingById(int id) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            BookingDAOJdbc daoJdbc = factory.getDAO(BookingDAOJdbc.class);
            return daoJdbc.getBookingById(id);
        } catch (DAOException e) {
            LOGGER.error("Cannot booking by id", e);
            throw new ServiceException("Cannot booking by id");
        }
    }
}
