package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.AbstractDAOFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.BookingDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.BookingHistoryDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.CarsDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.entity.Booking;
import ua.nure.manivchuk.model.entity.BookingHistory;

import java.util.List;

public class BookingHistoryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingHistoryService.class);

    public boolean createBookingHistory(int userId, int carId, String dateFrom, String dateTo, long totalPrice, String status){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            BookingHistoryDAOJdbc daoJdbc = factory.getDAO(BookingHistoryDAOJdbc.class);
            return daoJdbc.insertBookingHistoryCar(userId, carId, dateFrom, dateTo, totalPrice, status);
        } catch (DAOException e) {
            LOGGER.error("Cannot booking history car", e);
            throw new ServiceException("Cannot booking booking car");
        }
    }

    public List<BookingHistory> getAllBookingsHistoryByUserId(int user_id){
        try{
            AbstractDAOFactory factory = new DaoFactoryJdbc();
            BookingHistoryDAOJdbc bookingDAO = factory.getDAO(BookingHistoryDAOJdbc.class);
            return bookingDAO.getAllBookingHistoryByUserID(user_id);
        } catch (DAOException e) {
            throw new ServiceException("Cannot get all booking history");
        }
    }

    public BookingHistory getBookingHistoryByUserId(int user_id) {
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            BookingHistoryDAOJdbc daoJdbc = factory.getDAO(BookingHistoryDAOJdbc.class);
            return daoJdbc.getBookingHistoryById(user_id);
        } catch (DAOException e) {
            LOGGER.error("Cannot booking by user_id", e);
            throw new ServiceException("Cannot booking by user_id");
        }
    }
}
