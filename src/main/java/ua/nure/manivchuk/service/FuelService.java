package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.ClassificDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.dao.jdbc.FuelDAOJdbc;
import ua.nure.manivchuk.model.entity.Classific;
import ua.nure.manivchuk.model.entity.Fuel;

import java.util.ArrayList;

public class FuelService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FuelService.class);

    public ArrayList<Fuel> getAllFuels(){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            FuelDAOJdbc daoJdbc = factory.getDAO(FuelDAOJdbc.class);
            return daoJdbc.getAllFuels();
        } catch (DAOException e) {
            LOGGER.error("Cannot get all fuels", e);
            throw new ServiceException("Cannot get all fuels");
        }
    }
}
