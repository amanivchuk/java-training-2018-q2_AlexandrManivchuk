package ua.nure.manivchuk.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.dao.factory.DaoFactoryJdbc;
import ua.nure.manivchuk.model.dao.jdbc.CityDAOJdbc;
import ua.nure.manivchuk.model.dao.jdbc.DAOException;
import ua.nure.manivchuk.model.entity.City;

import java.util.List;

public class CityService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CityService.class);

    public List<City> getAllCity(){
        try{
            DaoFactoryJdbc factory = new DaoFactoryJdbc();
            CityDAOJdbc daoJdbc = factory.getDAO(CityDAOJdbc.class);
            return daoJdbc.getAllCity();
        } catch (DAOException e) {
            LOGGER.error("Cannot get all cities", e);
            throw new ServiceException("Cannot get all cities");
        }
    }

    public City getCityByName(String city) {
        return null;
    }
}
