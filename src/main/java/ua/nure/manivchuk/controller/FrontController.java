package ua.nure.manivchuk.controller;

import ua.nure.manivchuk.controller.action.Action;
import ua.nure.manivchuk.controller.action.ActionFactory;
import ua.nure.manivchuk.controller.action.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Servlet", urlPatterns = "/do/*")
@MultipartConfig(maxFileSize = 16177215)
public class FrontController extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontController.class);
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ActionFactory factory = new ActionFactory();
        Action action = factory.getAction(req);
        if(action == null){
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Not found");
            return;
        }
        processResult(action, req, resp);
    }

    private void processResult(Action action, HttpServletRequest req, HttpServletResponse resp){
        View inf;
        boolean isRedirect;
        try{
            inf = action.execute(req, resp);
            LOGGER.info("request page name is " + inf.getName());
            isRedirect = inf.isRedirect();
            if(isRedirect){
                resp.sendRedirect(req.getContextPath() + "/do/" + inf.getName());
                LOGGER.info("redirect to {} is processed ", req.getContextPath() + "/do/" + inf.getName());
            }else{
                String path = "/WEB-INF/jsp/" + inf.getName() + ".jsp";
                req.getRequestDispatcher(path).forward(req,resp);
                LOGGER.info("forward to {} is processed ", path);
            }
        } catch (ServletException | IOException e) {
            e.printStackTrace();
            LOGGER.error("Execution is failed", e);
            throw new RuntimeException("Execution is failed", e);
        }
    }
}
