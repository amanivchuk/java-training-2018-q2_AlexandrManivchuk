package ua.nure.manivchuk.controller.action;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.Photos;
import ua.nure.manivchuk.model.entity.UserChoosenProdect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet("/ShowPhotos")
public class ShowPhotosController extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ShowPhotosController.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        response.setContentType("image/jpeg");
        OutputStream out = response.getOutputStream();
        try{
            LOG.info("=== ShowPhoto===  START ===");
            LOG.info("index = " + request.getParameter("index"));
            int index = Integer.valueOf(request.getParameter("index"));
            LOG.info("index = " + index);

            UserChoosenProdect choose = (UserChoosenProdect) request.getSession().getAttribute("userChoose");
            Car car = choose.getCar() ;
            LOG.info("car = " + car);
            Photos photo = car.getPhotos().get(index);
            LOG.info("photo = " + photo);
            response.setContentLength(photo.getImage().length);
            out.write(photo.getImage());
            LOG.info("=== ShowPhoto===  END ===");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
