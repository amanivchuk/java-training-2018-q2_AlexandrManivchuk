package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteUserAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteUserAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        UserService service = new UserService();
        int id = Integer.parseInt(req.getParameter("id"));
        View redirectToAdminPage = new View("allUsersPageAdmin", REDIRECT);
        User user = service.getUserById(id);
        if(user.getRole().equals("User")){
            service.deleteUser(id);
            req.getSession().setAttribute("successDelete", "User delete successful");
            LOGGER.info("Username: " + user.getFirstName() + " delete successful");
        } else{
            req.getSession().setAttribute("failDelete", "Cannot delete admin");
        }
        return redirectToAdminPage;
    }
}
