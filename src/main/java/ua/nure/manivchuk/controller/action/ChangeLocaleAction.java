package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeLocaleAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChangeLocaleAction.class);

    /**
     * this method puts locale into cookie
     */

    @Override
    public View execute(HttpServletRequest request, HttpServletResponse response) {
        String language = request.getParameter("lang");
        LOGGER.info(request.getLocale().toString());
        if (language.equals("ru") || language.equals("en")) {
            Cookie locale = new Cookie("locale", language);
            response.addCookie(locale);
            LOGGER.info("locale in cookie is " + locale.getValue());
        }
        return new View(getReferrerName(request), REDIRECT);
    }

}
