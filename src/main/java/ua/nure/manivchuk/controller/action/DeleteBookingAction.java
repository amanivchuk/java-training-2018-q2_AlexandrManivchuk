package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Booking;
import ua.nure.manivchuk.service.BookingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteBookingAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteBookingAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        BookingService service = new BookingService();
        int id = Integer.parseInt(req.getParameter("id"));
        View redirectToAdminPage = new View("allBookingPageAdmin", REDIRECT);
        Booking booking = service.getBookingById(id);
        service.deleteBooking(id, booking);
        req.getSession().setAttribute("successDelete", "Car delete successful");
//        LOGGER.info("Car: " + car.getMark() + " delete successful");

        return redirectToAdminPage;
    }
}
