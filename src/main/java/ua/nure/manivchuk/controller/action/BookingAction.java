package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.UserChoosenProdect;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BookingAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        View view = new View("booking");
        HttpSession session = req.getSession();
        Car selectedCar = (Car) session.getAttribute("bookingCar");
        if(selectedCar != null){
            return view;
        } else {
            String car_id = req.getParameter("car");
            LOGGER.info("Booking selected car_id = " + car_id);

            List<Car> carList = (List<Car>) req.getSession().getAttribute("listCar");
            for (Car car : carList) {
                if (car.getId() == Integer.parseInt(car_id)) {
                    selectedCar = car;
                }
            }
            UserChoosenProdect userChoose = (UserChoosenProdect) req.getSession().getAttribute("userChoose");

            String dateFrom = userChoose.getDateFrom();
            String timeFrom = userChoose.getTimeFrom();
            String ffrom = dateFrom + " " + timeFrom + ":00";
            Timestamp from = Timestamp.valueOf(ffrom);
            String dateTo = userChoose.getDateTo();
            String timeTo = userChoose.getTimeTo();
            String tTo = dateTo + " " + timeTo + ":00";
            Timestamp to = Timestamp.valueOf(tTo);

            long different = to.getTime() - from.getTime();
            long totalTimeOnRent = TimeUnit.MILLISECONDS.toHours(different);
            LOGGER.info("==========================================>>>> total hour for rent = " + totalTimeOnRent);

            long totalPrice = totalTimeOnRent * selectedCar.getPrice();
            LOGGER.info("==========================================>>>> total hour for rent = " + totalPrice);

            userChoose.setCar(selectedCar);
            userChoose.setPrice(totalPrice);

            req.getSession().setAttribute("bookingCar", selectedCar);
            req.getSession().setAttribute("totalPrice", totalPrice);
            LOGGER.info("Selected car is " + selectedCar);
            return view;
        }
    }
}
