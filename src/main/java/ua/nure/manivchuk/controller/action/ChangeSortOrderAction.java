package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;

public class ChangeSortOrderAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChangeSortOrderAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        HashSet<String> listValueForSort = (HashSet<String>) req.getSession().getAttribute("listValueForSort");
        if (listValueForSort == null){
            listValueForSort = new HashSet<>();
        }

        String sortOrder=req.getParameter("sort-order");
//        req.getSession().setAttribute("sortOrder", sortOrder);
        LOGGER.info("sort order in this session is " + sortOrder);

        if(sortOrder.equals("noMatter"))
            listValueForSort.clear();
        if(sortOrder.equals("byPetrol"))
            listValueForSort.add("Бензин");
        if(sortOrder.equals("byDiesel"))
            listValueForSort.add("Дизель");
        if(sortOrder.equals("byGaz"))
            listValueForSort.add("Газ");
        if(sortOrder.equals("byHandKPP"))
            listValueForSort.add("Ручная КПП");
        if(sortOrder.equals("byAutoKPP"))
            listValueForSort.add("Автоматическая КПП");
        if(sortOrder.equals("byVariatorKPP"))
            listValueForSort.add("Вариатор");
        if(sortOrder.equals("byRobotKPP"))
            listValueForSort.add("Робот");
        if(sortOrder.equals("byVnedoroznik"))
            listValueForSort.add("Внедорожник");
        if(sortOrder.equals("byKabriolet"))
            listValueForSort.add("Кабриолет");
        if(sortOrder.equals("byKupe"))
            listValueForSort.add("Купе");
        if(sortOrder.equals("byLimuzin"))
            listValueForSort.add("Лимузин");
        if(sortOrder.equals("byMikroavtobus"))
            listValueForSort.add("Микроавтобус");
        if(sortOrder.equals("byMiniven"))
            listValueForSort.add("Минивэн");
        if(sortOrder.equals("byPikap"))
            listValueForSort.add("Пикап");
        if(sortOrder.equals("bySedan"))
            listValueForSort.add("Седан");
        if(sortOrder.equals("byUniversal"))
            listValueForSort.add("Универсал");
        if(sortOrder.equals("byFurgon"))
            listValueForSort.add("Фургон");
        if(sortOrder.equals("byHechback"))
            listValueForSort.add("Хетчбек");

        req.getSession().setAttribute("listValueForSort", listValueForSort);

        return new View(getReferrerName(req), REDIRECT);
    }
}
