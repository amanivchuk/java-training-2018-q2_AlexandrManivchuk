package ua.nure.manivchuk.controller.action;

import ua.nure.manivchuk.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyAutoAction extends Action {
    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {

        User user = (User) req.getSession().getAttribute("userAttributes");
        if(user == null){
            return new View("loginpage");
        }

        return new View("/user/addnewcar");
    }
}
