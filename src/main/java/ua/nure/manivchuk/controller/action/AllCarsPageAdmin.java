package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.service.CarService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AllCarsPageAdmin extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(AllCarsPageAdmin.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("All cars page showed successfully");

        View view;
        if(isAdmin(req)){
            CarService service = new CarService();
            List<Car> fragment = service.getAllCar();
            req.setAttribute("carsList", fragment);
            if(fragment.isEmpty()){
                req.setAttribute("emptyMessageCar", "Empty list");
                LOGGER.info("Car list is empty");
            }
            view = new View("/admin/allCarsShow");
        } else {
            LOGGER.info("Not admin!!!");
            view = REDIRECT_TO_HOME;
        }
        return view;
    }
}
