package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.controller.handling.CatalogHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;

public class ChangeSortByAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChangeSortByAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
       String sortBy = req.getParameter("sort-order");
       if(sortBy.equals("byName") || sortBy.equals("byPrice")){
           req.getSession().setAttribute("sortBy", sortBy);
           LOGGER.info("sort by in this session is" + sortBy);

//           CatalogHandler catalogHandler = new CatalogHandler();
//           catalogHandler.getRequestedList(req);

       }

        return new View(getReferrerName(req), REDIRECT);
    }
}
