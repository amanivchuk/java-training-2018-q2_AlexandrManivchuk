package ua.nure.manivchuk.controller.action;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.UserChoosenProdect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet("/ShowImageCheckout")
public class ShowImageCheckoutController extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ShowImageCheckoutController.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
        response.setContentType("image/jpeg");
        OutputStream out = response.getOutputStream();
        try{
            LOG.info("=== CarShowPhoto===  START ===");
            LOG.info("indexxxxxx = " + request.getParameter("index"));
            int index = Integer.parseInt(request.getParameter("index"));
            LOG.info("index = " + index);

            UserChoosenProdect choose = (UserChoosenProdect) request.getSession(false).getAttribute("userChoose");
            Car car = choose.getCar();
            LOG.info("car = " + car);
            response.setContentLength(car.getImage().length);
            out.write(car.getImage());
            LOG.info("=== CarShowPhoto===  END ===");

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
