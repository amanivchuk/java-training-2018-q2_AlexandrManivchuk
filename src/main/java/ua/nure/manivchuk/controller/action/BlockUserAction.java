package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BlockUserAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(BlockUserAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        UserService service = new UserService();
        int id = Integer.parseInt(req.getParameter("id"));
        int statusBlock = Integer.parseInt(req.getParameter("status"));
        View redirectToAdminPage = new View("allUsersPageAdmin", REDIRECT);
        User user = service.getUserById(id);
        if(user.getRole().equals("User")){
            service.blockUser(id, statusBlock);
            if(statusBlock == 1)
                req.getSession().setAttribute("successBlock", "User is block successful");
            if(statusBlock == 0)
                req.getSession().setAttribute("successUnBlock", "User is unblock successful");
            LOGGER.info("Username: " + user.getFirstName() + " is block successful");
        } else{
            req.getSession().setAttribute("failBlock", "Cannot block user");
        }
        return redirectToAdminPage;
    }
}
