package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.City;
import ua.nure.manivchuk.service.CityService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class HomePage extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePage.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("home-page showed successfully");

        req.getSession().removeAttribute("userChoose");
        req.getSession().removeAttribute("listCar");
        req.getSession().removeAttribute("beforeSortListCar");
        req.getSession().removeAttribute("bookingCar");
        req.getSession().removeAttribute("totalPrice");

        CityService cityService = new CityService();
        List<City> listCity = cityService.getAllCity();
        req.getSession(false).setAttribute("listCity", listCity);

        LOGGER.info("List city get from databace");

        return new View("home-page");
    }
}
