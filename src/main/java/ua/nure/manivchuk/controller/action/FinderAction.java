package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.controller.handling.CatalogHandler;
import ua.nure.manivchuk.controller.handling.FinderValidator;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.UserChoosenProdect;
import ua.nure.manivchuk.service.CarService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class FinderAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(FinderAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("FinderAction is working...");
        View view = new View("content");
        HttpSession session = req.getSession();
//        List<Car> beforeSortListCar = (List<Car>) req.getSession().getAttribute("beforeSortListCar");
//        req.getSession().setAttribute("listCar", beforeSortListCar);

        List<Car> listCar = (List<Car>) session.getAttribute("listCar");

        if(listCar !=null && listCar.size() > 0){

       /* if(beforeSortListCar!= null && beforeSortListCar.size() > 0 ){
            req.getSession().setAttribute("listCar", beforeSortListCar);

            HashSet<String> listValueForSort = (HashSet<String>) req.getSession().getAttribute("listValueForSort");
            Set<Car> cars = new HashSet<>();
            if(listValueForSort != null && !listValueForSort.isEmpty()){
                for(String s : listValueForSort){
                    for(Car car : beforeSortListCar){
                       if( s.equals(car.getFuel().getFuel())){
                            cars.add(car);
                        }
                        if(s.equals(car.getTransmission().getTransmission())) {
                            cars.add(car);
                        }
                        if(s.equals(car.getCarcase().getCarcase())) {
                            cars.add(car);
                        }
                    }
                }
                if(!cars.isEmpty()) {
                    List<Car> list = new ArrayList<>(cars);
                    req.getSession().setAttribute("listCar", list);
                }
            }*/

            CatalogHandler handler = new CatalogHandler();

        /*    CatalogByDriverHandler catalogHandler = new CatalogByDriverHandler();
            List<Car> listSort = catalogHandler.getRequestedList(req);
            if (listSort != null && !listSort.isEmpty()){
                LOGGER.info("--------------------------- listSort" + listSort);
                req.getSession().setAttribute("listSort", listSort);
            }
*/
            List<Car> list = handler.getRequestedList(req);
            LOGGER.info("Sorted list =======>>> " + list);
            req.getSession().setAttribute("listCar", list);

            return view;
        }else {
            String dateFrom = req.getParameter("date");
            String timeFrom = req.getParameter("timepicker1");
            String dateTo = req.getParameter("date2");
            String timeTo = req.getParameter("timepicker2");

            FinderValidator validator = new FinderValidator();
            if(validator.execute(req)) {

                UserChoosenProdect choose = new UserChoosenProdect();
                choose.setCity(req.getParameter("from_city"));
                choose.setDateFrom(dateFrom);
                choose.setTimeFrom(timeFrom);
                choose.setDateTo(dateTo);
                choose.setTimeTo(timeTo);

                CarService carService = new CarService();
                listCar = carService.getAllCarsByCity(choose.getCity());
                LOGGER.info(listCar.toString());
                req.getSession().setAttribute("listCar", listCar);
//            req.getSession().setAttribute("beforeSortListCar", listCar);
                req.getSession().setAttribute("userChoose", choose);

                return view;
            } else{
                LOGGER.info("finder failure");
                return new View(getReferrerName(req), REDIRECT);
            }
        }
    }
}
