package ua.nure.manivchuk.controller.action;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet("/ShowFotoDocument")
public class ShowFotoDocumentAction extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShowFotoDocumentAction.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        OutputStream out = response.getOutputStream();
        try{
            int user_id = (int) request.getAttribute("user_id");
            LOGGER.info("ShowFotoDocumentationAction user_id= " + user_id);
            UserService userService = new UserService();
            User user = userService.getUserById(user_id);
            LOGGER.info("==================== USERS ATTRIBUTES =" + user);

            int index = Integer.valueOf(request.getParameter("index"));
            LOGGER.info("==================== PHOTO DOCUMENT id=" + index);
            switch (index){
                case 1 : {
                    response.setContentLength(user.getDocument1().length);
                    out.write(user.getDocument1());
                    LOGGER.info("Get Document1 for current user=" + user);
                    break;
                }
                case 2 : {
                    response.setContentLength(user.getDocument2().length);
                    out.write(user.getDocument2());
                    LOGGER.info("Get Document2 for current user=" + user);
                    break;
                }
                case 3 : {
                    response.setContentLength(user.getDocument3().length);
                    out.write(user.getDocument3());
                    LOGGER.info("Get Document3 for current user=" + user);
                    break;
                }
                case 4 : {
                    response.setContentLength(user.getDocument4().length);
                    out.write(user.getDocument4());
                    LOGGER.info("Get Document4 for current user=" + user);
                    break;
                }
            }
        }catch (Exception e){
           /* e.printStackTrace();*/
        }finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
