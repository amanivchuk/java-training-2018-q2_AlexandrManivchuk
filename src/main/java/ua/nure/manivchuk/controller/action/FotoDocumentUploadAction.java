package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.CarService;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class FotoDocumentUploadAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(FotoDocumentUploadAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        if(!isAutorized(req)){
            InputStream passport1 = null;
            InputStream passport2 = null;
            InputStream vu1 = null;
            InputStream vu2 = null;
            View view = new View("user/mycar");
            int user_id = (int) req.getSession().getAttribute("userIdAttribute");

            UserService userService = new UserService();
            try {
                Part part1 = req.getPart("passport1");
                Part part2 = req.getPart("passport2");
                Part part3 = req.getPart("vu1");
                Part part4 = req.getPart("vu2");

                if(part1!= null &&part1.getSize() > 0){
                    LOGGER.info("==============================================> FILE DOCUMENT 1=========> "+ part1.getSize());
                    passport1 = part1.getInputStream();
                    String path = "PASSPORT1";
                    userService.updateFotoDocument(passport1, user_id, path);
                }
                if(part2!= null && part2.getSize() > 0){
                    LOGGER.info("==============================================> FILE DOCUMENT 2=========> "+ part2.getSize());
                    passport2 = part2.getInputStream();
                    String path = "PASSPORT2";
                    userService.updateFotoDocument(passport2, user_id, path);
                }
                if(part3!= null && part3.getSize() > 0){
                    vu1 = part3.getInputStream();
                    String path = "VU1";
                    userService.updateFotoDocument(vu1, user_id, path);
                }
                if(part4!= null &&  part4.getSize() > 0){
                    vu2 = part4.getInputStream();
                    String path = "VU2";
                    userService.updateFotoDocument(vu2, user_id, path);
                }
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }

            User updatedUser = userService.getUserById(user_id);

            req.getSession().setAttribute("userAttributes", updatedUser);
            req.getSession().setAttribute("successUpdateDocumentUser", "User documents update successful");

            CarService carService = new CarService();
            List<Car> usersListCar = carService.getAllUsersCarById(updatedUser.getId());
            req.setAttribute("userListCar", usersListCar);

            return view;
        } else{
            LOGGER.info("registration failure");
            req.getSession().setAttribute("failUpdateDocumentUser", "Cannot update user document");
            return new View(getReferrerName(req), REDIRECT);
        }
    }
}
