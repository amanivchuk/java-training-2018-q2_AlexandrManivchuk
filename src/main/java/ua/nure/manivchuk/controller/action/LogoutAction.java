package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogoutAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
       HttpSession session = req.getSession();
       session.setAttribute("userAttributes", null);
        session.setAttribute("userNameAttribute", null);
        session.setAttribute("userNameAttribute", null);
        session.setAttribute("userNameAttribute", null);
        LOGGER.info("logout successful");
        return REDIRECT_TO_HOME;

    }
}
