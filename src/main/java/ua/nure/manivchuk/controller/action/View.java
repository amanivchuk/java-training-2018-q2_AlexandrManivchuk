package ua.nure.manivchuk.controller.action;

public class View {
    private String name;
    private boolean isRedirect = false;

    public View(String name) {
        this.name = name;
    }

    public View(String name, boolean isRedirect) {
        this.name = name;
        this.isRedirect = isRedirect;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRedirect() {
        return isRedirect;
    }

    public void setRedirect(boolean redirect) {
        isRedirect = redirect;
    }
}
