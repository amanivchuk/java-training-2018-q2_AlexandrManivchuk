package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

public class ChangePasswordUserSettingsAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChangePasswordUserSettingsAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        if(!isAutorized(req)){
            InputStream file = null;
            View view = new View("user/setingsuser");
            String oldpassword = req.getParameter("oldpassword");
            String newpassword = req.getParameter("newpassword");
            String confpassword = req.getParameter("confpassword");


            User cuurentUser = (User) req.getSession().getAttribute("userAttributes");
            User updatedUser = null;
            if(cuurentUser.getPassword().equals(oldpassword)){
                if(newpassword.equals(confpassword)){
                    UserService userService = new UserService();
                    updatedUser = userService.updatePasswordUser(newpassword, cuurentUser);
                    req.getSession().setAttribute("successUpdatePaaswordUser", "Password update successful");
                    req.getSession().setAttribute("userAttributes", updatedUser);

                }else{
                    req.getSession().setAttribute("checkInputPassword", "Check new password with confirm password");
                    return view;
                }
            } else{
                req.getSession().setAttribute("oldPasswordNotEqualsNew", "Old password not equals new password");
                return view;
            }
            return view;
        } else{
            LOGGER.info("registration failure");
            req.getSession().setAttribute("failUpdatePassword", "Cannot update user password settings");
            return new View(getReferrerName(req), REDIRECT);
        }
    }
}
