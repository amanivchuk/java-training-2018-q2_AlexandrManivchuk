package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.CarService;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

public class ChangeUserSettings extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChangeUserSettings.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        if(!isAutorized(req)){
            InputStream file = null;
            View view = new View("user/setingsuser");
            String name = req.getParameter("name");
            String lastname = req.getParameter("lastname");
            String email = req.getParameter("email");
            String phone = req.getParameter("phone");
            try {
                Part profile = req.getPart("profile");
                LOGGER.info("!!!!!!profile=!!!!!!!!!=" + profile.getSize());
                if(profile.getSize() > 0){
                    file = profile.getInputStream();
                }
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
            User changedUser = new User();
            changedUser.setId((Integer) req.getSession().getAttribute("userIdAttribute"));
            changedUser.setFirstName(name);
            changedUser.setLastName(lastname);
            changedUser.setEmail(email);
            changedUser.setPhone(phone);

            UserService userService = new UserService();
            User updatedUser = null;
            if(file != null) {
                LOGGER.info("!!!!!!UpdateUserWithPhoto!!!!!!!!!");
                updatedUser = userService.updateUserWithPhoto(changedUser, file);
            } else {
                LOGGER.info("!!!!!!UpdateUser!!!!!!!!!");
                updatedUser = userService.updateUser(changedUser);
            }
            req.getSession().setAttribute("userAttributes", updatedUser);
            req.getSession().setAttribute("successUpdateUser", "User settings update successful");
            return view;
        } else{
            LOGGER.info("registration failure");
            req.getSession().setAttribute("failUpdateUser", "Cannot update user settings");
            return new View(getReferrerName(req), REDIRECT);
        }
    }
}
