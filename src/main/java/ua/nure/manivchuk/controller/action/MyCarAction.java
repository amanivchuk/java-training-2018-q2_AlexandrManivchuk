package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.CarService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class MyCarAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyCarAction.class);
    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        View view = new View("/user/mycar");

        User currentUser = (User) req.getSession().getAttribute("userAttributes");
        CarService carService = new CarService();
        List<Car> usersListCar = carService.getAllUsersCarById(currentUser.getId());
        LOGGER.info("userListCar =======---------->>> " + usersListCar);
        req.setAttribute("userListCar", usersListCar);
        req.getSession().setAttribute("listCar", usersListCar);
        return view;
    }
}
