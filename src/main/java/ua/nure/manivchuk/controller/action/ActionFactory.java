package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class ActionFactory {
    private static Map<String, Action> actions;
    private static final Logger LOGGER = LoggerFactory.getLogger(ActionFactory.class);

    public ActionFactory() {
        actions = new HashMap<>();
        actions.put("GET/home-page", new HomePage());
        actions.put("POST/finder", new FinderAction());
        actions.put("GET/finder", new FinderAction());
        actions.put("GET/login-page", new ShowLoginPage());
        actions.put("POST/login", new LoginAction());
        actions.put("GET/logout", new LogoutAction());
        actions.put("POST/booking", new BookingAction());
        actions.put("GET/booking", new BookingAction());
        actions.put("POST/checkout", new CheckoutAction());
        actions.put("GET/checkout", new CheckoutAction());
        actions.put("POST/confirmBooking", new ConfirmBookingAction());
        actions.put("POST/inform", new ConfirmBookingAction());
        actions.put("GET/autorization", new AutorizationAction());
        actions.put("GET/registration", new RegistrationAction());
        actions.put("GET/allUsersPageAdmin", new AllUsersPageAdmin());
        actions.put("GET/delete-user", new DeleteUserAction());
        actions.put("GET/allCarsPageAdmin", new AllCarsPageAdmin());
        actions.put("GET/delete-car", new DeleteCarAction());
        actions.put("GET/allBookingPageAdmin", new AllBookingPageAdmin());
        actions.put("GET/delete-booking", new DeleteBookingAction());
        actions.put("GET/myauto", new MyAutoAction());
        actions.put("GET/addnewcar", new AddNewCarAction());
        actions.put("POST/addnewcaraction", new AddNewUserCarAction());
        actions.put("GET/mycar", new MyCarAction());
        actions.put("GET/myorders", new MyOrdersAction());
        actions.put("GET/setinsuser", new SetingsUserAction());
        actions.put("POST/changeusersettings", new ChangeUserSettings());
        actions.put("POST/changepassword", new ChangePasswordUserSettingsAction());
        actions.put("POST/fotoDocumentUpload", new FotoDocumentUploadAction());
        actions.put("GET/change-order", new ChangeSortOrderAction());
        actions.put("GET/change-by", new ChangeSortByAction());
        actions.put("GET/sort-by", new ChangeSortByDriverAction());
        actions.put("GET/block-user", new BlockUserAction());
        actions.put("POST/rejectBooking", new RejectBookingAction());
        actions.put("GET/locale", new ChangeLocaleAction());
        actions.put("POST/register", new RegisterAction());
    }

    public Action getAction(HttpServletRequest req){
        LOGGER.info("request key is " + req.getMethod() + req.getPathInfo());
        return actions.get(req.getMethod() + req.getPathInfo());
    }
}
