package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.service.CarService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeleteCarAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteCarAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        CarService service = new CarService();
        int id = Integer.parseInt(req.getParameter("id"));
        View redirectToAdminPage = new View("allCarsPageAdmin", REDIRECT);
        Car car = service.getCarById(id);
        service.deleteCar(id);
        req.getSession().setAttribute("successDelete", "Car delete successful");
        LOGGER.info("Car: " + car.getMark() + " delete successful");

        return redirectToAdminPage;
    }
}
