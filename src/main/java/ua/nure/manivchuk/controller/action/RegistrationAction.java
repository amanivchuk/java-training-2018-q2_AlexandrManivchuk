package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegistrationAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("Redirect to registration page");
        return new View("registration");
    }
}
