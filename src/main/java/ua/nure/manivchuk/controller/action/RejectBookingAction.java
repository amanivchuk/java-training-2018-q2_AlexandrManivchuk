package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.controller.handling.MailHandler;
import ua.nure.manivchuk.model.entity.Booking;
import ua.nure.manivchuk.service.BookingService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RejectBookingAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(RejectBookingAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        BookingService service = new BookingService();
        int id = Integer.parseInt(req.getParameter("id"));
        String message = req.getParameter("comment");
        LOGGER.info("user id = " + id);
        LOGGER.info("COMMENT " + message);
        View redirectToAdminPage = new View("allBookingPageAdmin", REDIRECT);
        Booking booking = service.getBookingById(id);
        service.deleteBooking(id, booking);
        req.getSession().setAttribute("successDelete", "Car cannot confirm by manager");
        MailHandler.sendMail(booking.getUser(), booking.getCar(), booking.getFrom().toString(), booking.getTo().toString(), booking.getTotalPrice(), message);
LOGGER.info("Reject booking action done.");
        return redirectToAdminPage;
    }
}
