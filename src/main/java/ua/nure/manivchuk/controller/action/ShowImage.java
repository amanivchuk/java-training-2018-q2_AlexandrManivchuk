package ua.nure.manivchuk.controller.action;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

@WebServlet("/ShowImage")
public class ShowImage extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShowImage.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpeg");
        OutputStream out = response.getOutputStream();
        try{
            int index = Integer.valueOf(request.getParameter("index"));
            LOGGER.info("INDEX ==== " + index);

            ArrayList<Car> list = (ArrayList<Car>) request.getSession(false).getAttribute("listCar");
            Car car = list.get(index);
            response.setContentLength(car.getImage().length);
            out.write(car.getImage());
            LOGGER.info("Get image for current car=" + car);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req,resp);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
