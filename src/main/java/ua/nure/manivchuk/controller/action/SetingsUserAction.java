package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.CarService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

public class SetingsUserAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetingsUserAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        if(!isAutorized(req)){
            View view = new View("/user/setingsuser");
            return view;
        } else{
            LOGGER.info("registration failure");
            return new View(getReferrerName(req), REDIRECT);
        }
    }
}
