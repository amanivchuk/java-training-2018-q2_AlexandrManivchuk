package ua.nure.manivchuk.controller.action;

import ua.nure.manivchuk.model.entity.*;
import ua.nure.manivchuk.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddNewCarAction extends Action {
    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {

        CityService cityService = new CityService();
        List<City> listCity = cityService.getAllCity();
        req.getSession(false).setAttribute("listCity", listCity);

        ClassificService classificService = new ClassificService();
        List<Classific> listClassific = classificService.getAllClassifics();
        req.getSession(false).setAttribute("listClassific", listClassific);

        RulesrentService rulesrentService = new RulesrentService();
        List<Rulesrent> listRulesrent = rulesrentService.getAllClassifics();
        req.getSession(false).setAttribute("listRulesrent", listRulesrent);

        TransmissionService transmissionService = new TransmissionService();
        List<Transmission> listTransmission = transmissionService.getAllTransmissons();
        req.getSession(false).setAttribute("listTransmission", listTransmission);

        FuelService fuelService = new FuelService();
        List<Fuel> listFuel = fuelService.getAllFuels();
        req.getSession(false).setAttribute("listFuel", listFuel);

        CarcaseService carcaseService = new CarcaseService();
        List<Carcase> listCarcase = carcaseService.getAllCarcases();
        req.getSession(false).setAttribute("listCarcase", listCarcase);




        return new View("/user/addnewcar");
    }
}
