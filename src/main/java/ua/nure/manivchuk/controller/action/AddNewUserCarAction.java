package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.CarService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

public class AddNewUserCarAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddNewUserCarAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        if(!isAutorized(req)){
            InputStream file1 = null;
            InputStream file2 = null;
            InputStream file3 = null;
            View view = new View("/user/addnewcar");
            String carName = req.getParameter("car_name");
            String city = req.getParameter("city");
            try {
                Part filePart1 = req.getPart("fil");
                Part filePart2 = req.getPart("file2");
                LOGGER.info("========================================>>>>> file1 = " + filePart1.getName());
                file1 = filePart1.getInputStream();
                file2 = filePart2.getInputStream();
                file3 = filePart1.getInputStream();
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
            String[] rulesrentArray = req.getParameterValues("rulesrent");
            String transmission = req.getParameter("transmission");
            String classific = req.getParameter("classific");
            String carcase = req.getParameter("carcase");
            String fuel = req.getParameter("fuel");
            String price = req.getParameter("price");

            User user = (User) req.getSession().getAttribute("userAttributes");

            CarService carService = new CarService();
            carService.createCar(carName, city, rulesrentArray, transmission, classific, fuel, price, user, carcase, file1, file2, file3);

            req.getSession().removeAttribute("listCar");
            return view;
        } else{
            LOGGER.info("registration failure");
            return new View(getReferrerName(req), REDIRECT);
        }
    }
}
