package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.controller.handling.CatalogByDriverHandler;
import ua.nure.manivchuk.controller.handling.CatalogHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeSortByDriverAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChangeSortByDriverAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
       String sortByDriver = req.getParameter("sort-order");
        if(sortByDriver.equals("withDriver") || sortByDriver.equals("withoutDriver")){
           req.getSession().setAttribute("sortByDriver", sortByDriver);
           LOGGER.info("sort by driver in this session is" + sortByDriver);

       }

        return new View(getReferrerName(req), REDIRECT);
    }
}
