package ua.nure.manivchuk.controller.action;

import ua.nure.manivchuk.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public abstract class Action {
    public static final UserService USER_SERVICE = new UserService();

    public static final boolean REDIRECT = true;
    public static final View REDIRECT_TO_HOME = new View("home-page", REDIRECT);
    private static final Logger LOGGER = LoggerFactory.getLogger(Action.class);

    public abstract View execute(HttpServletRequest req, HttpServletResponse resp);

    public boolean isAdmin(HttpServletRequest req){
        HttpSession session = req.getSession(false);
        if(session != null && session.getAttribute("userRoleAttribute") != null){
            return session.getAttribute("userRoleAttribute").equals("Admin");
        } else{
            return false;
        }
    }

    public boolean isUser(HttpServletRequest req){
        HttpSession session = req.getSession(false);
        if(session.getAttribute("userRoleAttribute") != null){
            return session != null && session.getAttribute("userRoleAttribute").equals("User");
        } else{
            return false;
        }
    }

    public boolean isAutorized(HttpServletRequest req){
        HttpSession session = req.getSession(false);
        return session != null && session.getAttribute("userId") != null;
    }

    public String getReferrerName(HttpServletRequest req){
        String name = req.getHeader("referer").substring(25);
        name = name.replace(".jsp", "");
        LOGGER.info("referrer name:" + name);
        return name;
    }

    public boolean isInteger(HttpServletRequest req, String key){
        try{
            Integer.parseInt(req.getParameter(key));
        }catch (NumberFormatException e){
            return false;
        }
        return true;
    }

}
