package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckoutAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckoutAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        Car choosenCar = (Car) req.getSession().getAttribute("bookingCar");
        User user = (User) req.getSession().getAttribute("userAttributes");
        LOGGER.info("============checkout.do=============");
        LOGGER.info("===>> " + "  " + choosenCar);
        LOGGER.info("--> " + user);
        LOGGER.info("===>> " + "  " + req.getSession().getAttribute("dateFrom"));
        LOGGER.info("===>> " + "  " + req.getSession().getAttribute("timeFrom"));
        LOGGER.info("===>> " + "  " + req.getSession().getAttribute("dateTo"));
        LOGGER.info("===>> " + "  " + req.getSession().getAttribute("timeTo"));
        LOGGER.info("============checkout.do======END=======");
        String dateFrom = req.getSession().getAttribute("dateFrom") + " " + req.getSession().getAttribute("timeFrom");
        String dateTo = req.getSession().getAttribute("dateTo") + " " + req.getSession().getAttribute("timeTo");


      /*  boolean resultBooking = Manager.getInstance().insertBooking(user.getId(), choosenCar.getId(), dateFrom ,dateTo);
        if(resultBooking){
            boolean result = Manager.getInstance().insertChangeStatusCar(choosenCar.getId());
            if(result){
                LOG.info("Car " + choosenCar.getMark() + " was booked by" + user.getLogin());

                String nextJSP = "/inform.do";
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
                dispatcher.forward(req,resp);
            }
        }*/

        return new View("checkout");
    }
}
