package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.controller.handling.RegisterValidator;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RegisterAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        if(!isAutorized(req)){
            View view = new View("registration");
            RegisterValidator validator = new RegisterValidator();
            UserService service = new UserService();
            String firstName = req.getParameter("name");
            String lastName = req.getParameter("lastname");
            String email = req.getParameter("email");
            String password = req.getParameter("password");
            if(validator.execute(req)){
                User user = new User();
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setPassword(password);
                service.registerUser(user);
                String message = "Register successful, you can authorize now";
                req.getSession().setAttribute("message", message);
                view.setName("home-page");
                LOGGER.info("registration successfully");
                view.setRedirect(true);
            }
            return view;
        } else {
            LOGGER.info("registration failure");
            return new View(getReferrerName(req), REDIRECT);
        }
    }
}
