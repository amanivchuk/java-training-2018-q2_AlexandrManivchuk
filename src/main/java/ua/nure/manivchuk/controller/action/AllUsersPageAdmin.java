package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.City;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.CityService;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AllUsersPageAdmin extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(AllUsersPageAdmin.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("All users page showed successfully");

        View view;
        if(isAdmin(req)){
            UserService service = new UserService();
            List<User> fragment = service.getAllUser();
            req.setAttribute("usersList", fragment);
            if(fragment.isEmpty()){
                req.setAttribute("emptyMessage", "Empty list");
                LOGGER.info("User list is empty");
            }
            view = new View("/admin/allUsersShow");
        } else {
            LOGGER.info("Not admin!!!");
            view = REDIRECT_TO_HOME;
        }
        return view;
    }
}
