package ua.nure.manivchuk.controller.action;

import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        String email = req.getParameter("email");
        String password = req.getParameter("password");


        UserService service = new UserService();
        User user;
        View redirectToReferrer = new View(getReferrerName(req), REDIRECT);
        boolean isValidPair = service.isEmailExist(email) && service.isValidPair(email, password);
        if (isValidPair) {
            user = service.getUserByEmail(email);
            if(!user.getStatus()){
                req.getSession(false).setAttribute("userAttributes", user);
                req.getSession(false).setAttribute("userRoleAttribute", user.getRole());
                req.getSession(false).setAttribute("userIdAttribute", user.getId());
                LOGGER.info("User role = " + user.getRole());
                LOGGER.info(user.getFirstName() + " " + user.getLastName() + " logged successfully");
            }
            session.setAttribute("userStatus", "User is block by Administrator");
//            redirectToReferrer = new View("home-page");
        } else {
            session.setAttribute("loginError", "Invalid Login or Password");
            LOGGER.info("authorization is failed");
        }

        if(redirectToReferrer.getName().equals("loginpage.jsp"))
            return new View("/user/addnewcar");

        return redirectToReferrer;

    }
}
