package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.controller.handling.MailHandler;
import ua.nure.manivchuk.model.entity.BookingHistory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.model.entity.UserChoosenProdect;
import ua.nure.manivchuk.service.BookingHistoryService;
import ua.nure.manivchuk.service.BookingService;
import ua.nure.manivchuk.service.CarService;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConfirmBookingAction extends Action {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfirmBookingAction.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("Confirm booking action!!!");
        User user = (User) req.getSession().getAttribute("userAttributes");
        BookingService bookingService = new BookingService();
        CarService carService = new CarService();
        UserChoosenProdect userChoose = (UserChoosenProdect) req.getSession().getAttribute("userChoose");
        Car car = userChoose.getCar();
        String dateFrom = userChoose.getDateFrom() + " " + userChoose.getTimeFrom();
        String dateTo = userChoose.getDateTo() + " " + userChoose.getTimeTo();
        long totalPrice = userChoose.getPrice();

        if(user != null){
            LOGGER.info("user = " + user);
            LOGGER.info("user exist!");
            if (bookingCar(user, bookingService, carService, car, dateFrom, dateTo, totalPrice)){
                cleanCurrentSession(req);

                MailHandler.sendMail(user, car, dateFrom, dateTo, totalPrice);
                return REDIRECT_TO_HOME;
            }
        } else {
            String nameUser = req.getParameter("name");
            String surname = req.getParameter("surname");
            String email = req.getParameter("email");
            String phone = req.getParameter("tel");
            LOGGER.info("new User = " + nameUser + " " + surname + " " + email + " " + phone);
            User newUser = new User(nameUser, surname, email, phone, phone);
            UserService userService = new UserService();
            long user_id = userService.registerUser(newUser);
            newUser.setId((int) user_id);
            LOGGER.info("new user was registered: " + newUser);
            if (bookingCar(newUser, bookingService, carService, car, dateFrom, dateTo, totalPrice)) {
                cleanCurrentSession(req);
                return REDIRECT_TO_HOME;
            }
            else return new View("error");

        }
        return new View("error");
    }

    private void cleanCurrentSession(HttpServletRequest req) {
        req.getSession().removeAttribute("listCar");
        req.getSession().removeAttribute("userChoose");
    }

    private boolean bookingCar(User user, BookingService bookingService, CarService carService, Car car, String dateFrom, String dateTo, long totalPrice) {
        boolean resultBooking = bookingService.createBooking(user.getId(), car.getId(), dateFrom, dateTo, totalPrice);

        BookingHistoryService historyService = new BookingHistoryService();
        historyService.createBookingHistory(user.getId(), car.getId(), dateFrom, dateTo, totalPrice, "Booked");

        if(resultBooking){
            LOGGER.info("Car was booking");
            if(carService.updateCarStatus(car.getId())){
                LOGGER.info("Car was changed status on booking");

                return true;
            }
        }
        return false;
    }
}
