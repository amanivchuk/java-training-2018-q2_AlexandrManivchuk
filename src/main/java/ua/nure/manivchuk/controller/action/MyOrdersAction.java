package ua.nure.manivchuk.controller.action;

import ua.nure.manivchuk.model.entity.BookingHistory;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.BookingHistoryService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class MyOrdersAction extends Action {
    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        User user = (User) req.getSession().getAttribute("userAttributes");

        BookingHistoryService bookingHistoryService = new BookingHistoryService();
        List bookingHistory = bookingHistoryService.getAllBookingsHistoryByUserId(user.getId());

        req.setAttribute("bookingHistory", bookingHistory);

        return new View("/user/myorders");
    }
}
