package ua.nure.manivchuk.controller.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShowLoginPage extends Action {
    private final static Logger LOGGER = LoggerFactory.getLogger(ShowLoginPage.class);

    @Override
    public View execute(HttpServletRequest req, HttpServletResponse resp) {
        return new View("login-page");
    }
}
