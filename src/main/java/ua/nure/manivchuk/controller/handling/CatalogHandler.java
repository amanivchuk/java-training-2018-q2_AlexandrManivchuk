package ua.nure.manivchuk.controller.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CatalogHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogHandler.class);
    private static final int PAGE_VOLUME = 10;

    public List<Car> getRequestedList(HttpServletRequest request){
        List<Car> listCar = null;

        String sortOrder = (String) request.getSession().getAttribute("sortBy");
        if(sortOrder != null){
            listCar = (List<Car>) request.getSession().getAttribute("listCar");
            listCar = resultSort(listCar, sortOrder);
        }
        /*List<Car> listSort = (List<Car>) request.getSession().getAttribute("listSort");
        if(sortOrder != null){
            if(listSort!= null && !listSort.isEmpty()){
                return resultSort(listSort, sortOrder);
            } else {
                listCar = (List<Car>) request.getSession().getAttribute("listCar");
                listCar = resultSort(listCar, sortOrder);
            }
        }*/
        LOGGER.info("SORT BY NAME RESULT =====-========> " + listCar);
        return listCar;
    }

    private List<Car> resultSort(List<Car> listCar, String sortOrder){
        if(sortOrder != null){
            if(sortOrder.equals("byName")){
                listCar = sortByName(listCar);
            }
            if(sortOrder.equals("byPrice")){
                listCar = sortByPrice(listCar);
            }
        }
        return listCar;
    }


    private List<Car> sortByPrice(List<Car> listCar) {
        Collections.sort(listCar, new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return Integer.compare(o1.getPrice(), o2.getPrice());
            }
        });
        return listCar;
    }

    private List<Car> sortByName(List<Car> listCar) {
        Collections.sort(listCar, new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getMark().compareToIgnoreCase(o2.getMark());
            }
        });
        return listCar;
    }
}
