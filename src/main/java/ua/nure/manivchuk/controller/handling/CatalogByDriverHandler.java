package ua.nure.manivchuk.controller.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.Rulesrent;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class CatalogByDriverHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogByDriverHandler.class);

    public List<Car> getRequestedList(HttpServletRequest request){
        List<Car> listCar = (List<Car>) request.getSession().getAttribute("beforeSortListCar");
        String sortOrderDriver = (String) request.getSession().getAttribute("sortByDriver");
        List<Car> sorterCar = null;

        if(sortOrderDriver != null){
            if(sortOrderDriver.equals("withDriver")){
                sorterCar = sortByDriver(listCar);
            }
            if(sortOrderDriver.equals("withoutDriver")){
                sorterCar = sortByWithoutDriver(listCar);
            }
        }
//        request.getSession().setAttribute("listCar", sorterCar);

        LOGGER.info("SORT BY NAME RESULT =====-========> " + listCar);

        return sorterCar;
    }

    private List<Car> sortByDriver(List<Car> listCar) {
        List<Car> list = new ArrayList<>();

        for(Car car : listCar){
            LOGGER.info("---------------------- "+ car.getRulesrent());
            for(Rulesrent r : car.getRulesrent())
                if(r.getRulerent().equals("С водителем"))
                    list.add(car);
        }
        return list;
    }

    private List<Car> sortByWithoutDriver(List<Car> listCar) {
        List<Car> list = new ArrayList<>();
        for(Car car : listCar){
            for(Rulesrent r : car.getRulesrent())
                if(r.getRulerent().equals("Без водителя"))
                    list.add(car);
        }
        return list;
    }
}
