package ua.nure.manivchuk.controller.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class FinderValidator extends Validator {
    private static final Logger LOGGER = LoggerFactory.getLogger(FinderValidator.class);
//    This method validates registration form and sends necessary message

    public boolean execute(HttpServletRequest req){
        String dateFrom = req.getParameter("date").trim();
        String timeFrom = req.getParameter("timepicker1").trim();
        String dateTo = req.getParameter("date2").trim();
        String timeTo = req.getParameter("timepicker2").trim();
        LOGGER.info("dateFrom ==>" + dateFrom);
        LOGGER.info("timeFrom ==>" + timeFrom );
        LOGGER.info("dateTo ==>" + dateTo);
        LOGGER.info("timeTo ==>" + timeTo);

        List<String> message = new ArrayList<>();
        if(!isStringMatches(getRegex("notEmpty"), dateFrom)){
            message.add("dateFromEmpty");
        }
        if(!isStringMatches(getRegex("notEmpty"), timeFrom)){
            message.add("timeFromEmpty");
        }
        if(!isStringMatches(getRegex("notEmpty"), dateTo)){
            message.add("dateToEmpty");
        }
        if(!isStringMatches(getRegex("notEmpty"), timeTo)){
            message.add("timeToEmpty");
        }
        if(!message.isEmpty()){
            sendMessagesBySessionAttribute(message, req);
        }
        return message.isEmpty();

    }
}
