package ua.nure.manivchuk.controller.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.model.entity.Car;
import ua.nure.manivchuk.model.entity.User;
import ua.nure.manivchuk.service.BookingService;
import ua.nure.manivchuk.service.CarService;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MailHandler.class);

    public static void sendMail(User user, Car car, String dateFrom, String dateTo, long totalPrice) {

        try {
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", "465");
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "true");
            properties.put("mail.smtp.socketFactory.port", "465");

            Session session = Session.getInstance(properties,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication("phenomeneee@gmail.com", "HP_DV6700");
                        }
                    });
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("phenomeneee@gmail.com"));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(user.getEmail()));
                message.setSubject("Booking rent car");
                StringBuilder sb = new StringBuilder();
                sb.append("Dear ").append(user.getFirstName()).append(" ").append(user.getLastName()).
                        append(", your booked ").append(car.getMark()).append(" in period from: ").append(dateFrom)
                        .append(" to ").append(dateTo).append(". Your price is ").append(totalPrice).append("$.")
                        .append(" Our manager call you soon for confirm your booking.");
                message.setText(sb.toString());
                Transport.send(message);

                LOGGER.info("Mail with registration info was sent to " + user.getEmail());
            } catch (MessagingException e) {
                LOGGER.info("Cant sent mail " + e);
                throw new RuntimeException(e);
            }
            LOGGER.info("success MAIL");
        } catch (Exception ex) {
            LOGGER.error(String.valueOf(ex));
        }
    }

    public static void sendMail(User user, Car car, String dateFrom, String dateTo, int totalPrice, String messageManager) {
        try {
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", "465");
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "true");
            properties.put("mail.smtp.socketFactory.port", "465");

            Session session = Session.getInstance(properties,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication("phenomeneee@gmail.com", "HP_DV6700");
                        }
                    });
            try {
                LOGGER.info("MESSAGE ---- > " + messageManager);
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("phenomeneee@gmail.com"));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(user.getEmail()));
                message.setSubject("Booking rent car");
                StringBuilder sb = new StringBuilder();
                sb.append("Dear ").append(user.getFirstName()).append(" ").append(user.getLastName()).
                        append(", your booked ").append(car.getMark()).append(" in period from: ").append(dateFrom)
                        .append(" to ").append(dateTo).append(". Your price is ").append(totalPrice).append("$. ")
                        .append(messageManager);
                message.setText(sb.toString());
                Transport.send(message);

                LOGGER.info("Mail with registration info was sent to " + user.getEmail());
            } catch (MessagingException e) {
                LOGGER.info("Cant sent mail " + e);
                throw new RuntimeException(e);
            }
            LOGGER.info("success MAIL");
        } catch (Exception ex) {
            LOGGER.error(String.valueOf(ex));
        }
    }

}

