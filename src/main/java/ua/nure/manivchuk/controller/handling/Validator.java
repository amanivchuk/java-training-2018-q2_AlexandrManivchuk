package ua.nure.manivchuk.controller.handling;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
     * Validator subclasses help to handle request and validate forms, send necessary messages to jsp
     */
public abstract class Validator {
    public static final int STRING_LIMIT = 30;



    public String getRegex(String key) {
        ResourceBundle bundle = ResourceBundle.getBundle("regex");
        return bundle.getString(key);
    }

    public boolean isStringMatches(String regex, String string) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }

    public boolean isInteger(HttpServletRequest request, String key) {
        try {
            Integer.parseInt(request.getParameter(key));
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public void sendMessagesByRequestAttribute(List<String> messages, HttpServletRequest request) {
        for (String key : messages) {
            request.setAttribute(key, "msg");
        }
    }
    public void sendMessagesBySessionAttribute(List<String> messages, HttpServletRequest request) {
        for (String key : messages) {
            request.getSession().setAttribute(key, "msg");
        }
    }

}
