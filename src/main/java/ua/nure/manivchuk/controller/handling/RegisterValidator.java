package ua.nure.manivchuk.controller.handling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.nure.manivchuk.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class RegisterValidator extends Validator {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterValidator.class);
//    This method validates registration form and sends necessary message

    public boolean execute(HttpServletRequest req){
        UserService service = new UserService();
        String firstName = req.getParameter("name").trim();
        String lastName = req.getParameter("lastname").trim();
        String email = req.getParameter("email").trim();
        String password = req.getParameter("password").trim();
        String confirmPassword = req.getParameter("confirmPassword").trim();
        List<String> message = new ArrayList<>();
        if(!isStringMatches(getRegex("notEmpty"), firstName)){
            message.add("firstNameEmpty");
        } else if(!isStringMatches(getRegex("name"), firstName) || firstName.length() > STRING_LIMIT){
            message.add("invalidFirstName");
        }
        if(!isStringMatches(getRegex("notEmpty"), lastName)){
            message.add("lastNameEmpty");
        } else if(!isStringMatches(getRegex("name"), lastName) || lastName.length() > STRING_LIMIT){
            message.add("invalidLastName");
        }
        if(!isStringMatches(getRegex("notEmpty"), email)){
            message.add("emailEmpty");
        } else if(!isStringMatches(getRegex("email"), email) || email.length() > STRING_LIMIT){
            message.add("invalidEmail");
        } else if(service.isEmailExist(email)) {
            message.add("busyEmail");
        }
        if(!isStringMatches(getRegex("notEmpty"), password)){
            message.add("passwordIsEmpty");
        } else if(!isStringMatches(getRegex("password"), password) || password.length() > STRING_LIMIT){
            message.add("invalidPassword");
        } else if (!password.equals(confirmPassword)){
            message.add("confirmError");
        }
        if(!message.isEmpty()){
            sendMessagesByRequestAttribute(message, req);
        }
        return message.isEmpty();

    }
}
