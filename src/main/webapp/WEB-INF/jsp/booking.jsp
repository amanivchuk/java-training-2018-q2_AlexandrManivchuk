<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">
    <fmt:message key="booking.cost_per_hour" var="cost_per_hour"/>
    <fmt:message key="booking.hour_price" var="hour_price"/>
    <fmt:message key="booking.total_cost" var="total_cost"/>
    <fmt:message key="booking.info" var="info"/>
    <fmt:message key="booking.receiving" var="receiving"/>
    <fmt:message key="booking.ending" var="ending"/>
    <fmt:message key="booking.reserve" var="reserve"/>
    <fmt:message key="booking.vehicle_features" var="vehicle_features"/>
    <fmt:message key="booking.body_type" var="body_type"/>
    <fmt:message key="booking.additional_service" var="additional_service"/>
    <fmt:message key="booking.description" var="description"/>
</fmt:bundle>

<t:genericpage>
    <jsp:attribute name="content2">
        <c:set var="price" value="${sessionScope.userChoose.getCar().getPrice()}"/>
        <c:set var="transmission" value="${sessionScope.userChoose.getCar().getTransmission().getTransmission()}"/>
        <c:set var="fuel" value="${sessionScope.userChoose.getCar().getFuel().getFuel()}"/>
        <c:set var="classific" value="${sessionScope.userChoose.getCar().getClassific().getClassific()}"/>

        <div class="global-wrap container-fluid">
            <div class="row">
                <div class="container">
                    <div class="booking-item-details">
                        <div class="booking-item-details-car">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 menu-carousel">
                                        <div id="carousel" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <div class="carousel-indicators-wrap">
                                                <ol class="carousel-indicators">
                                                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                                                    <li data-target="#carousel" data-slide-to="1"></li>
                                                    <li data-target="#carousel" data-slide-to="2"></li>
                                                </ol>
                                            </div>

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                                <div class="item active">
                                                    <div class="bgslide" style="background-image: url(${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowPhotos?index=0)"></div>
                                                    <div class="carousel-caption">
                                                        ...
                                                    </div>
                                                </div>
                                                <c:forEach begin="1" end="${sessionScope.userChoose.getCar().getPhotos().size()-1}" varStatus="id">
                                                <div class="item">
                                                    <c:set var="req" value="${pageContext.request}" />
                                                    <c:set var="url">${req.requestURL}</c:set>
                                                    <c:set var="uri" value="${req.requestURI}" />
                                                    <div class="bgslide" style="background-image: url(${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowPhotos?index=${id.index})"></div>
                                                    <div class="carousel-caption">
                                                        ...
                                                    </div>
                                                </div>
                                                </c:forEach>
                                            </div>

                                            <!-- Controls -->
                                            <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="my-row-3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="content-my-row-1">
                                                        <ul class="list">
                                                            <li>
                                                                <p>
                                                                    <span class="left-span">${cost_per_hour}</span>
                                                                    <span>${sessionScope.userChoose.getCar().getPrice()}${hour_price}</span>
                                                                </p>
                                                            </li>
                                                           <%-- <li>
                                                                <p>
                                                                    <span class="left-span">Цена за 2 дня </span>
                                                                    <span class="ar_full_price right-span" data-value="0">3 200 ₽</span>
                                                                </p>
                                                            </li>--%>
                                                            <li>
                                                                <p>
                                                                    <span class="left-span">${total_cost}</span>
                                                                    <span class="st_data_car_total right-span">${sessionScope.userChoose.getPrice()}$</span>
                                                                </p>
                                                                <div class="spinner cars_price_img_loading "></div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="description">
                                                        <div class="content-my-row-1">
                                                            <p>${info}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="content-my-row-1">
                                                    <div class="col-md-6">
                                                        <p> ${receiving}</p>
                                                        <div class="info-rent__receipt-date" style="font-weight: bold">${sessionScope.userChoose.dateFrom}, ${sessionScope.userChoose.timeFrom}</div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p>${ending}</p>
                                                        <div class="info-rent__end-date" style="font-weight: bold"> ${sessionScope.userChoose.dateTo}, ${sessionScope.userChoose.timeTo} </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <form action="checkout" method="post"><%--
                                                            <button type="button" class="btn btn-primary btn-lg btn-block">Бронировать</button>--%>
                                                                <button type="submit" class="btn btn-primary btn-lg btn-block">${reserve}</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="description-auto">
                                            <div class="col-md-3">
                                                <h4>${vehicle_features}</h4>
                                                <ul>
                                                    <li class="">
                                                        <i class="im im-shift-auto"></i>
                                                        <span class="booking-item-feature-title">${transmission}</span>
                                                    </li>
                                                    <li class="">
                                                        <i class="im im-diesel"></i>
                                                        <span class="booking-item-feature-title">${fuel}</span>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <h4>${body_type}</h4>
                                                <ul>
                                                    <li class="">
                                                        <i class="im im-shift-auto"></i>
                                                        <span class="booking-item-feature-title">${classific}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <h4>${hour_price}</h4>
                                                <ul>
                                                    <li class="">
                                                        <i class="im im-shift-auto"></i>
                                                        <span class="booking-item-feature-title">${price}$</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <h4>${additional_service}</h4>
                                                <ul>
                                                    <c:forEach var="rul" varStatus="loop" items="${userChoose.getCar().getRulesrent()}">
                                                        <li class="">
                                                            <i class="fa fa-automobile"></i>
                                                            <span class="booking-item-feature-title">${rul.rulerent}</span>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="description-auto-full">
                                            <h4>${description}</h4>
                                            <span itemprop="description"> <p style="margin-bottom: 40px;">АВТОрент поможет вам <strong>арендовать автомобиль <c:out  value="${sessionScope.userChoose.getCar().getMark()}"/>, <c:out  value="${sessionScope.userChoose.getCar().getYear()}"/></strong> в городе <c:out  value="${sessionScope.userChoose.getCar().getCity().getCity()}"/> по цене <c:out  value="${sessionScope.userChoose.getCar().getPrice()}"/> ₽ в день. Также возможны скидки в зависимости от продолжительности аренды авто.
                                            <c:out  value="${sessionScope.userChoose.getCar().getMark()}"/>, <c:out  value="${sessionScope.userChoose.getCar().getYear()}"/> предоставляется с залогом ${price} ₽ в кузове <c:out  value="${sessionScope.userChoose.getCar().getCarcase().getCarcase()}"/>,
                                            со следующими характеристиками: ${transmission}, тип топлива: ${fuel}.
                                            Подача автомобиля возможна: подача в аэропорт, с водителем или без водителя.
                                        </p></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </jsp:attribute>
</t:genericpage>
