<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">
    <fmt:message key="checkout.acceprt_booking" var="acceprt_booking"/>
    <fmt:message key="checkout.accept_booking_info" var="accept_booking_info"/>
    <fmt:message key="checkout.accept" var="accept"/>
    <fmt:message key="checkout.name" var="name"/>
    <fmt:message key="checkout.surname" var="surname"/>
    <fmt:message key="checkout.telephone" var="telephone"/>
    <fmt:message key="checkout.confirm" var="confirm"/>
    <fmt:message key="checkout.amount_paid" var="amount_paid"/>
    <fmt:message key="checkout.receiving" var="receiving"/>
    <fmt:message key="checkout.ending" var="ending"/>
</fmt:bundle>

<t:genericpage>
    <jsp:attribute name="content3">
    <div class="global-wrap container-fluid">
        <div class="row">
            <div class="container">
                <div class="booking-item-details">
                    <div class="booking-item-details-car">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 user-form">
                                    <h4>${acceprt_booking}</h4>
                                    <p>${accept_booking_info}</p>
                                   <c:if test = "${not empty sessionScope.userAttributes.role}">
                                            <form action="confirmBooking" method="post" class="form-horizontal">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-default">${accept}</button>
                                                </div>
                                            </form>
                                   </c:if>

                                    <c:if test = "${empty sessionScope.userAttributes.role}">
                                        <form action="confirmBooking" method="post" class="form-horizontal">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" id="name" placeholder="${name}">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="surname" id="surname" placeholder="${surname}">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <div class="form-group help">
                                                <input type="text" class="form-control" name="tel" id="tel"
                                                       placeholder="${telephone}">
                                                <i class="fa fa-lock"></i>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-default">${confirm}</button>
                                            </div>
                                        </form>
                                    </c:if>
                                </div>
                                <div class="col-md-6">
                                    <div class="my-row-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="content-my-row-1-2">
                                                    <c:set var="req" value="${pageContext.request}" />
                                                    <c:set var="url">${req.requestURL}</c:set>
                                                    <c:set var="uri" value="${req.requestURI}" />
                                                   <%-- <img  src="<%=request.getContextPath()%>/ShowImageCheckout?index=<%=car.getId()%>"/>--%>
                                                       <img  src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowImageCheckout?index=${sessionScope.userChoose.getCar().getId()}"/>
                                                    <ul class="list">
                                                        <li>
                                                            <p>
                                                                <span><c:out  value="${sessionScope.userChoose.getCar().getMark()}"/>, <c:out  value="${sessionScope.userChoose.getCar().getYear()}"/></span>
                                                            </p>
                                                        </li>
                                                        <li>
                                                            <p>
                                                                <span class="left-span">${amount_paid}</span>
                                                                <span class="ar_full_price right-span" data-value="0"> ${sessionScope.userChoose.price}$</span>
                                                            </p>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="my-border">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="content-my-row-1">
                                                <div class="col-md-6">
                                                    <p>${receiving}</p>
                                                    <div class="info-rent__receipt-date" style="font-weight: bold">
                                                            ${sessionScope.userChoose.dateFrom},  ${sessionScope.userChoose.timeFrom}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <p>${ending}</p>
                                                    <div class="info-rent__end-date" style="font-weight: bold">
                                                            ${sessionScope.userChoose.dateTo},  ${sessionScope.userChoose.timeTo}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </jsp:attribute>
</t:genericpage>
