<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">

    <fmt:message key="allusershow.name" var="name"/>
    <fmt:message key="allusershow.surname" var="surname"/>
    <fmt:message key="allusershow.phone" var="phone"/>
    <fmt:message key="allusershow.role" var="role"/>
    <fmt:message key="allusershow.status" var="status"/>
    <fmt:message key="allusershow.delete" var="delete"/>
    <fmt:message key="allusershow.block" var="block"/>
    <fmt:message key="allusershow.unblock" var="unblock"/>

    <c:if test="${not empty successDelete}">
        <c:set value="User delete success" var="VsuccessDelete"/>
    </c:if>
    <c:if test="${not empty failDelete}">
        <c:set value="User delete fail" var="VfailDelete"/>
    </c:if>
    <c:if test="${not empty successBlock}">
        <c:set value="User is block" var="blockStatus"/>
    </c:if>
    <c:if test="${not empty successUnBlock}">
        <c:set value="User is unblock" var="unblockStatus"/>
    </c:if>
    <c:if test="${not empty failBlock}">
        <c:set value="Cannot block user" var="failBlock"/>
    </c:if>
    <c:if test="${not empty emptyMessage}">
        <c:set value="Empty user list" var="varEmptyMessage"/>
    </c:if>
</fmt:bundle>

<t:genericpage>
    <jsp:attribute name="content">

        <div class="global-wrap container-fluid">
            <div class="row">
                <div class="container">

                    <span class="label-success">${VsuccessDelete}</span>
                    <span class="label-warning">${VfailDelete}</span>
                    <span class="label-warning">${blockStatus}</span>
                    <span class="label-warning">${unblockStatus}</span>
                    <span class="label-warning">${failBlock}</span>
                    <c:remove var="successDelete" scope="session"/>
                    <c:remove var="failDelete" scope="session"/>
                    <c:remove var="successBlock" scope="session"/>
                    <c:remove var="successUnBlock" scope="session"/>
                    <c:remove var="failBlock" scope="session"/>

                    <p>
                        <strong>Information about all register users</strong>
                    </p>
                    ${varEmptyMessage}
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>Email</th>
                            <th>${name}</th>
                            <th>${surname}</th>
                            <th>${phone}</th>
                            <th>${role}</th>
                            <th>${status}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${usersList}" var="user">
                        <tr>
                            <td><c:out value="${user.email}"/></td>
                            <td><c:out value="${user.firstName}"/></td>
                            <td><c:out value="${user.lastName}"/></td>
                            <td><c:out value="${user.phone}"/></td>
                            <td><c:out value="${user.role}"/></td>
                            <c:if test="${user.status}">
                                <c:set value="Block" var="statusB"/>
                                <td><c:out value="${statusB}"/></td>
                            </c:if>
                            <c:if test="${!user.status}">
                                <c:set value="Unblock" var="status"/>
                                <td><c:out value="${status}"/></td>
                            </c:if>
                            <td><a href="<c:url value="/do/delete-user?id=${user.id}"/>">${delete}</a></td>
                            <td><a href="<c:url value="/do/block-user?id=${user.id}&status=1"/>">${block}</a></td>
                            <td><a href="<c:url value="/do/block-user?id=${user.id}&status=0"/>">${unblock}</a></td>
                        </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
            </jsp:attribute>
</t:genericpage>