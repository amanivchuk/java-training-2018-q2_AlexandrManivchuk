<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generiusercpage>
    <jsp:attribute name="content">

                    <h1>Мои автомобили</h1>
                    <div class="content">
                        <div class="product">
                            <div class="products-row">
                                <div class="col-sm-4 product-img-desc">
                                    <div class="product-img">
                                        <a href="#"><img  src="img/goods1.jpg" alt="goods"></a>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="product-description">
                                        <p class="product-title">Nissan Almera, 2016</p>
                                        <p class="product-desc">Седан. Залог 15000$</p>
                                        <p class="product-price">1600 $/дн</p>
                                        <div class="product-option">
                                            <ul>
                                                <li>Автоматическая КПП</li>
                                                <li>Бензин</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

    </jsp:attribute>
</t:generiusercpage>
