<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:attribute name="content4">
    <div class="global-wrap container-fluid">
        <div class="row">
            <div class="container">
                <div class="booking-item-details">
                    <div class="booking-item-details-car">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h1>Машина забронирована</h1>
                                    <p>После подтверждения с Вами свяжется наш представитель для уточнения деталей и способа
                                        оплаты</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </jsp:attribute>
</t:genericpage>