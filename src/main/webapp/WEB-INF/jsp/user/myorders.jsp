<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">
    <fmt:message key="myorders.title" var="title"/>
    <fmt:message key="myorders.car" var="car"/>
    <fmt:message key="myorders.from" var="from"/>
    <fmt:message key="myorders.to" var="to"/>
    <fmt:message key="myorders.price" var="price"/>
    <c:if test="${not empty emptyMessageOrder}">
        <c:set value="Empty user list" var="varEmptyMessageCar"/>
    </c:if>
</fmt:bundle>

<t:generiusercpage>
    <jsp:attribute name="content">
        <div class="container">
            <p>
                <strong>${title}</strong>
            </p>
                ${varEmptyMessageOrder}
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>${car}</th>
                    <th>${from}</th>
                    <th>${to}</th>
                    <th>${price}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${bookingHistory}" var="booking">
                        <tr>
                            <td><c:out value="${booking.getCar().getMark()}"/></td>
                            <td><c:out value="${booking.from}"/></td>
                            <td><c:out value="${booking.to}"/></td>
                            <td><c:out value="${booking.totalPrice}"/></td>
                        </tr>
                        </c:forEach>
                </tbody>
            </table>
        </div>
    </jsp:attribute>
</t:generiusercpage>
