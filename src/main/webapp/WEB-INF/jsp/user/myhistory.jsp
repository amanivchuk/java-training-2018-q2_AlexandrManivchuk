<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">
    <fmt:message key="myhistort.title" var="title"/>
    <fmt:message key="myorders.car" var="car"/>
    <fmt:message key="myhistory.year" var="year"/>
    <fmt:message key="myhistory.price" var="price"/>
    <fmt:message key="myhistory.booked" var="booked"/>
    <fmt:message key="myhistorty.city" var="city"/>
    <c:if test="${not empty emptyMessageOrder}">
        <c:set value="Empty user list" var="varEmptyMessageCar"/>
    </c:if>
</fmt:bundle>

<t:generiusercpage>
    <jsp:attribute name="content">
        <div class="container">
            <p>
                <strong>${title}</strong>
            </p>
                ${varEmptyMessageOrder}
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>${car}</th>
                    <th>${year}</th>
                    <th>${price}</th>
                    <th>${booked}</th>
                    <th>${city}</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${carsList}" var="car">
                        <tr>
                            <td><c:out value="${car.mark}"/></td>
                            <td><c:out value="${car.year}"/></td>
                            <td><c:out value="${car.price}"/></td>
                            <td><c:out value="${car.availability}"/></td>
                            <td><c:out value="${car.getCity().getCity()}"/></td>
                        </tr>
                        </c:forEach>
                </tbody>
            </table>
        </div>
    </jsp:attribute>
</t:generiusercpage>
