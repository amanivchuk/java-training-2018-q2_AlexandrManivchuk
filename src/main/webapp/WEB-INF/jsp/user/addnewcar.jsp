<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">
    <fmt:message key="addnewcar.title" var="title"/>
    <fmt:message key="addnewcar.name" var="name"/>
    <fmt:message key="addnewcar.city" var="city"/>
    <fmt:message key="addnewcar.foto1" var="foto1"/>
    <fmt:message key="addnewcar.foto2" var="foto2"/>
    <fmt:message key="addnewcar.category" var="category"/>
    <fmt:message key="content.transmission" var="transmission"/>
    <fmt:message key="content.fuel" var="fuel"/>
    <fmt:message key="addnewcar.body" var="body"/>
    <fmt:message key="addnewcar.price" var="price"/>
    <fmt:message key="addnewcar.create" var="create"/>
</fmt:bundle>

<t:generiusercpage>
    <jsp:attribute name="content">

                    <h1>${title}</h1>
        <div class="addnewcar">
            <div class="card-body">

                <!-- Material form register -->
                <form method="post" action="addnewcaraction"  enctype="multipart/form-data">
                    <!-- Material input text -->
                    <div class="md-form">
                        <i class="fa fa-user prefix grey-text"></i>
                        <label for="materialFormCardNameEx" class="font-weight-light">${name}</label>
                        <input type="text" id="materialFormCardNameEx" class="form-control" name="car_name">
                    </div>
                    <br/>
                     <div class="md-form">
                        <i class="fa fa-envelope prefix grey-text"></i>
                        <select tabindex="1" data-placeholder="Select here.." class="form-control" name="city">
                            <option value="">${city}</option>
                            <c:forEach var="city" items="${listCity}">
                                <option data-country="${city.data}" value="${city.city}">${city.city}, ${city.country}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <br/>
                    <div class="md-form">
                        <label for="exampleFormControlFile1">${foto1}</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="fil">
                    </div>
                    <br/>
                    <div class="md-form">
                        <label for="exampleFormControlFile2">${foto2}</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile2" name="file2">
                    </div>
                    <br/>
                    <div class="md-form">
                        <select tabindex="1" data-placeholder="Select here.." class="form-control" name="classific">
                            <option value="">${category}</option>
                            <c:forEach var="classific" items="${listClassific}">
                                <option data-country="${classific.classific}" value="${classific.classific}">${classific.classific}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <br/>

                    <c:forEach var="rulesrent" items="${listRulesrent}">
                         <div class="form-check">
                             <input class="form-check-input" type="checkbox" value="${rulesrent.rulerent}" id="defaultCheck1" name="rulesrent">
                             <label class="form-check-label" for="defaultCheck1" >${rulesrent.rulerent}</label>
                         </div>
                    </c:forEach>

                    <br>
                    <div class="md-form">
                        <select tabindex="1" data-placeholder="Select here.." class="form-control" name="transmission">
                            <option value="">${transmission}</option>
                            <c:forEach var="transmission" items="${listTransmission}">
                                <option data-country="${transmission.transmission}" value="${transmission.transmission}">${transmission.transmission}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <br>
                    <div class="md-form">
                        <select tabindex="1" data-placeholder="Select here.." class="form-control" name="fuel">
                            <option value="">${fuel}</option>
                            <c:forEach var="fuel" items="${listFuel}">
                                <option data-country="${fuel.fuel}" value="${fuel.fuel}">${fuel.fuel}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <br>
                    <div class="md-form">
                        <select tabindex="1" data-placeholder="Select here.." class="form-control" name="carcase">
                            <option value="">${body}</option>
                            <c:forEach var="carcase" items="${listCarcase}">
                                <option data-country="${carcase.carcase}" value="${carcase.carcase}">${carcase.carcase}</option>
                            </c:forEach>
                        </select>
                    </div>

                    <bt/>
                    <div class="md-form">
                        <i class="fa fa-user prefix grey-text"></i>
                        <label for="materialFormCardNameE" class="font-weight-light">${price}</label>
                        <input type="text" id="materialFormCardNameE" class="form-control" name="price">
                    </div>

                    <div class="text-center py-4 mt-3">
                        <button class="btn btn-cyan" type="submit">${create}</button>
                    </div>
                </form>
                <!-- Material form register -->

            </div>
        </div>
<!-- Card -->

    </jsp:attribute>
</t:generiusercpage>
