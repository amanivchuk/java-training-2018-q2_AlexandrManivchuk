<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="i18n">
    <fmt:message key="setinguser.seting" var="seting"/>
    <fmt:message key="setinguser.name" var="name"/>
    <fmt:message key="setinguser.surname" var="surname"/>
    <fmt:message key="setinguser.phhone" var="phone"/>
    <fmt:message key="setinguser.photo" var="photo"/>
    <fmt:message key="setinguser.save_setings" var="save_setings"/>
    <fmt:message key="setinguser.current_password" var="current_password"/>
    <fmt:message key="setinguser.new_password" var="new_password"/>
    <fmt:message key="setinguser.confirm_password" var="confirm_password"/>
    <fmt:message key="setinguser.change_password" var="change_password"/>

    <c:if test="${not empty successUpdateUser}">
        <c:set value="User settings update success" var="successUpdate"/>
    </c:if>
    <c:if test="${not empty failUpdateUser}">
        <c:set value="User delete fail" var="failDelete"/>
    </c:if>

    <c:if test="${not empty successUpdatePaaswordUser}">
        <c:set value="User password update success" var="successUpdatePassword"/>
    </c:if>
    <c:if test="${not empty checkInputPassword}">
        <c:set value="Check new password with confirm password" var="checkInputPassword"/>
    </c:if>
    <c:if test="${not empty oldPasswordNotEqualsNew}">
        <c:set value="Old password not equals new password" var="oldPasswordNotEqualsNew"/>
    </c:if>
    <c:if test="${not empty failUpdatePassword}">
        <c:set value="Cannot update user password settings" var="failUpdatePassword"/>
    </c:if>
</fmt:bundle>

<t:generiusercpage>
    <jsp:attribute name="content">
        <h1>${seting}</h1>
        <div class="setings-user">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <span class="label-success">${successUpdate}</span>
                        <span class="label-warning">${failDelete}</span>
                        <c:remove var="successUpdateUser" scope="session"/>
                        <c:remove var="failUpdateUser" scope="session"/>
                        <form method="post" action="changeusersettings" enctype="multipart/form-data">
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="name" class="font-weight-light">${name}</label>
                                <input type="text" id="name" class="form-control" name="name" value="${sessionScope.userAttributes.getFirstName()}">
                            </div>
                            <br/>
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="lastname" class="font-weight-light">${surname}</label>
                                <input type="text" id="lastname" class="form-control" name="lastname" value="${sessionScope.userAttributes.getLastName()}">
                            </div>
                            <br/>
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="mail" class="font-weight-light">Email</label>
                                <input type="text" id="mail" class="form-control" name="email" value="${sessionScope.userAttributes.getEmail()}">
                            </div>
                            <br/>
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="phone" class="font-weight-light">${phone}</label>
                                <input type="text" id="phone" class="form-control" name="phone" value="${sessionScope.userAttributes.getPhone()}">
                            </div>
                            <br/>
<%--
                            <div class="md-form">
                                <i class="fa fa-envelope prefix grey-text"></i>
                                <select tabindex="1" data-placeholder="Select here.." class="form-control" name="city">
                                    <option value="">Выберите город</option>
                                    <c:forEach var="city" items="${listCity}">
                                    <option data-country="${city.data}"
                                            value="${city.city}">${city.city}, ${city.country}</option>
                                </c:forEach>
                                </select>
                            </div>
                            <br/>
--%>
                            <div class="col-xs-6 col-md-3">
                                <c:set var="req" value="${pageContext.request}" />
                                <c:set var="url">${req.requestURL}</c:set>
                                <c:set var="uri" value="${req.requestURI}" />
                                <a href="#"><img src="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/ShowFotoUser?index=${sessionScope.userIdAttribute}" alt="goods"></a>
                            </div>
                            <div class="md-form">
                                <label for="exampleFormControlFile1">${photo}</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="profile">
                            </div>
                            <br/>
                            <div class="text-center py-4 mt-3">
                                <button class="btn btn-cyan" type="submit">${save_setings}</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form method="post" action="changepassword">
                            <span class="label-success">${successUpdatePaaswordUser}</span>
                            <span class="label-warning">${checkInputPassword}</span>
                            <span class="label-warning">${oldPasswordNotEqualsNew}</span>
                            <span class="label-warning">${failUpdatePassword}</span>

                            <c:remove var="successUpdatePassword" scope="session"/>
                            <c:remove var="checkInputPassword" scope="session"/>
                            <c:remove var="oldPasswordNotEqualsNew" scope="session"/>
                            <c:remove var="failUpdatePassword" scope="session"/>
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="opass" class="font-weight-light">${current_password}</label>
                                <input type="text" id="opass" class="form-control" name="oldpassword">
                            </div>
                            <br/>
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="npass" class="font-weight-light">${new_password}</label>
                                <input type="text" id="npass" class="form-control" name="newpassword">
                            </div>
                            <br/>
                            <div class="md-form">
                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="cpass" class="font-weight-light">${confirm_password}</label>
                                <input type="text" id="cpass" class="form-control" name="confpassword">
                            </div>
                            <br/>
                            <div class="text-center py-4 mt-3">
                                <button class="btn btn-cyan" type="submit">${change_password}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </jsp:attribute>
</t:generiusercpage>
