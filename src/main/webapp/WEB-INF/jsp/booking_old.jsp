<%@ page contentType="text/html;charset=utf-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage>
    <jsp:attribute name="content2">
    <div class="global-wrap container-fluid">
        <div class="row">
         <%--   <%
                Car car = (Car) request.getAttribute("car");
                session.setAttribute("currentBookingCar", car);
            %>--%>
            <div class="container">
                <div class="booking-item-details">
                    <div class="booking-item-details-car">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 menu-carousel">
                                    <div id="carousel" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <div class="carousel-indicators-wrap">
                                            <ol class="carousel-indicators">
                                                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                                                <li data-target="#carousel" data-slide-to="1"></li>
                                                <li data-target="#carousel" data-slide-to="2"></li>
                                            </ol>
                                        </div>

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <div class="bgslide" style="background-image: url(<%--<%=request.getContextPath()%>/ShowPhotos?index=<%=0%>--%>)"></div>
                                                <div class="carousel-caption">
                                                    ...
                                                </div>
                                            </div>
                                            <%--<%for(int id = 1; id < car.getPhotos().size(); id++){%>--%>
                                                <div class="item">
                                                    <!--<img src="..." alt="...">-->
                                                    <c:set var="req" value="${pageContext.request}" />
                                                    <c:set var="url">${req.requestURL}</c:set>
                                                    <c:set var="uri" value="${req.requestURI}" />
                                                    <div class="bgslide" style="background-image: url(<%--<%=request.getContextPath()%>/ShowPhotos?index=<%=id%>--%>)"></div>
                                                    <div class="carousel-caption">
                                                        ...
                                                    </div>
                                                </div>
                                             <%--<%}%>--%>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="my-row-3">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="content-my-row-1">
                                                    <ul class="list">
                                                        <li>
                                                            <p>
                                                                <span class="left-span">Стоимость в день</span>
                                                                <span>${sessionScope.bookingCar.getPrice }<%--<%=car.getPrice()%>--%> $/дн.</span>
                                                            </p>
                                                        </li>
                                                        <li>
                                                            <p>
                                                                <span class="left-span">Цена за 2 дня </span>
                                                                <span class="ar_full_price right-span" data-value="0">3 200 ₽</span>
                                                            </p>
                                                        </li>
                                                        <li>
                                                            <p>
                                                                <span class="left-span">Прокат всего</span>
                                                                <span class="st_data_car_total right-span"> 3 200 ₽</span>
                                                            </p>
                                                            <div class="spinner cars_price_img_loading "></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="description">
                                                    <div class="content-my-row-1">
                                                        <p>После подтверждения бронирования с Вами свяжется наш представитель для уточнения деталей аренды и способа оплаты.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="content-my-row-1">
                                                <div class="col-md-6">
                                                    <p> Получение</p>
                                                    <div class="info-rent__receipt-date" style="font-weight: bold"> ${sessionScope.get("dateFrom")}, ${sessionScope.get("timeFrom")} </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <p> Окончание</p>
                                                    <div class="info-rent__end-date" style="font-weight: bold"> ${sessionScope.get("dateTo")}, ${sessionScope.get("timeTo")} </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form action="/checkout.do" method="post"><%--
                                                            <button type="button" class="btn btn-primary btn-lg btn-block">Бронировать</button>--%>
                                                            <button type="submit" class="btn btn-primary btn-lg btn-block">Бронировать</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="description-auto">
                                        <div class="col-md-3">
                                            <h4>Характеристики автомобиля</h4>
                                            <ul>
                                                <li class="">
                                                    <i class="im im-shift-auto"></i>
                                                    <span class="booking-item-feature-title">${sessionScope.bookingCar.getTransmission().getTransmission() }<%--<%=car.getTransmission().getTransmission()%>--%></span>
                                                </li>
                                                <li class="">
                                                    <i class="im im-diesel"></i>
                                                    <span class="booking-item-feature-title">${sessionScope.bookingCar.getFuel().getFuel()}</span>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <h4>Тип кузова</h4>
                                            <ul>
                                                <li class="">
                                                    <i class="im im-shift-auto"></i>
                                                    <span class="booking-item-feature-title">${sessionScope.bookingCar.getClassific().getClassific() }</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <h4>Залог</h4>
                                            <ul>
                                                <li class="">
                                                    <i class="im im-shift-auto"></i>
                                                    <span class="booking-item-feature-title">${sessionScope.bookingCar.price} ₽</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <h4>Доп. услуги</h4>
                                            <ul>
                                                <li class="">
                                                    <i class="fa fa-automobile"></i>
                                                    <span class="booking-item-feature-title">Без водителя</span>
                                                </li>
                                                <li class="">
                                                    <i class="fa fa-plane"></i>
                                                    <span class="booking-item-feature-title">Подача в аэропорт</span>
                                                </li>
                                                <li class="">
                                                    <i class="im im-driver"></i>
                                                    <span class="booking-item-feature-title">С водителем</span>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="description-auto-full">
                                        <h4>Описание</h4>
                                        <span itemprop="description"> <p style="margin-bottom: 40px;">АВТОрент поможет вам <strong>арендовать автомобиль ${sessionScope.bookingCar.mark}, ${sessionScope.bookingCar.year}</strong> в городе ${sessionScope.bookingCar.getCity().getCity()} по цене ${sessionScope.bookingCar.price} ₽ в день. Также возможны скидки в зависимости от продолжительности аренды авто.
                                            ${sessionScope.bookingCar.mark}, ${sessionScope.bookingCar.year} предоставляется с залогом ${sessionScope.bookingCar.price} ₽ в кузове ${sessionScope.bookingCar.getCarcase().getCarcase()}%>,
                                            со следующими характеристиками: ${sessionScope.bookingCar.getTransmission().getTransmission()}, тип топлива: ${sessionScope.bookingCar.getFuel().getFuel()}.
                                            Подача автомобиля возможна: подача в аэропорт, с водителем или без водителя.
                                        </p></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </jsp:attribute>
</t:genericpage>
